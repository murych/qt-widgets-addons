#include <QApplication>
#include <QTimer>
#include <QWidget>

#include "led.h"

#include <stdlib.h>

class LedExample : public QWidget
{
  Q_OBJECT
protected:
  QTimer timer;
  addons::Led* leds[2 * 3 * 2];
  const int led_width {16};
  const int led_height {10};
  const int grid {3};
  addons::Led::Shape shape;
  addons::Led::Look look;
  addons::Led::State state;
  int x;
  int y;
  int index;

  QTimer t_toggle;
  QTimer t_color;
  QTimer t_look;

  addons::Led* led;

  int led_color {0};
  addons::Led::Look led_look {addons::Led::Flat};

public:
  explicit LedExample(QWidget* parent = nullptr);
  ~LedExample();

  bool led_round {false};

public slots:
  void timeout();
  void nextColor();
  void nextLook();
};

LedExample::LedExample(QWidget* parent)
    : QWidget {parent}
{
  if (led_round) {
    led = new addons::Led {Qt::green, this};
    led->resize(30, 30);
    led->setShape(addons::Led::Circular);

    led->move(5, 5);

    t_toggle.setSingleShot(false);
    t_toggle.start(500);

    t_color.setSingleShot(false);
    t_color.start(750);

    t_look.setSingleShot(false);
    t_look.start(1000);

    connect(&t_toggle, &QTimer::timeout, led, &addons::Led::toggle);
    connect(&t_color, &QTimer::timeout, this, &LedExample::nextColor);
    connect(&t_look, &QTimer::timeout, this, &LedExample::nextLook);

    led->show();
    resize(240, 140);
  } else {
    y = grid;
    index = 0;

    for (int shape = 0; shape < 2; shape = (addons::Led::Shape)(shape + 1)) {
      x = grid;
      for (int look = 0; (int)look < 3; look = (addons::Led::Look)(look + 1)) {
        for (state = addons::Led::Off; (int)state < 2;
             state = (addons::Led::State)(state + 1))
        {
          leds[index] = new addons::Led {Qt::yellow,
                                         state,
                                         (addons::Led::Look)(look + 1),
                                         (addons::Led::Shape)(shape + 1),
                                         this};
          leds[index]->setGeometry(x, y, led_width, led_height);
          ++index;
          x += grid + led_width;
        }
      }
      y += grid + led_height;
    }
    setFixedSize(x + grid, y + grid);
    connect(&timer, &QTimer::timeout, this, &LedExample::timeout);
    timer.start(500);
  }
}

LedExample::~LedExample()
{
  if (led_round) {
    delete led;
  }
}

void LedExample::timeout()
{
  const auto NoOfLeds {sizeof(leds) / sizeof(leds[0])};
  //  auto count {0};
  // -----
  for (int count = 0; count < NoOfLeds; ++count) {
    if (leds[count]->state() == addons::Led::Off) {
      leds[count]->setState(addons::Led::On);
    } else {
      leds[count]->setState(addons::Led::Off);
    }
  }
}

void LedExample::nextColor()
{
  led_color++;
  led_color %= 4;

  switch (led_color) {
    default:
    case 0:
      led->setColor(Qt::green);
      break;
    case 1:
      led->setColor(Qt::blue);
      break;
    case 2:
      led->setColor(Qt::red);
      break;
    case 3:
      led->setColor(Qt::yellow);
      break;
  }
}

void LedExample::nextLook()
{
  int tmp {0};
  if (led_round) {
    tmp = (static_cast<int>(led_look) + 1) % 3;
  } else {
    tmp = (static_cast<int>(led_look) + 1) % 3;
  }
  led_look = static_cast<addons::Led::Look>(tmp);
  led->setLook(led_look);
  // qDebug("painting look %i", ledlook);
  // l->repaint();
}

int main(int argc, char** argv)
{
  QApplication app {argc, argv};
  LedExample widget;
  widget.show();
  return QApplication::exec();
}

#include "led_example.moc"
