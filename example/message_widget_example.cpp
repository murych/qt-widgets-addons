/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2012 Aurélien Gâteau <agateau@kde.org>
    Based on test program by Dominik Haumann <dhaumann@kde.org>

 SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <QAction>
#include <QApplication>
#include <QCheckBox>
#include <QFrame>
#include <QVBoxLayout>
#include <QWhatsThis>
#include <QWidget>

#include "message_widget.h"

class Receiver : public QObject
{
  Q_OBJECT
public:
  Receiver(QObject* parent)
      : QObject(parent)
  {
  }
  ~Receiver() override {}

public Q_SLOTS:
  void showWhatsThis(const QString& text)
  {
    QWhatsThis::showText(QCursor::pos(), text);
  }
};

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);

  auto* mainWindow = new QWidget;

  auto* l = new QVBoxLayout {mainWindow};

  auto* mw = new addons::MessageWidget {mainWindow};
  mw->setMessageType(addons::MessageWidget::MessageType::Positive);
  mw->setWordWrap(true);
  mw->setText(QStringLiteral(
      "Test KMessageWidget is properly sized when <a href=\"this is the "
      "contents\">word-wrap</a> is enabled by default."));
  mw->setIcon(QIcon::fromTheme(QStringLiteral("kde")));

  auto* mw2 = new addons::MessageWidget {mainWindow};
  mw2->setWordWrap(true);
  mw2->setText(QStringLiteral(
      "A KMessageWidget with an icon and two additional buttons"));
  mw2->setIcon(QIcon::fromTheme(QStringLiteral("kde")));
  mw2->addAction(new QAction(QStringLiteral("Foo"), mw2));
  mw2->addAction(new QAction(QStringLiteral("Bar"), mw2));

  auto* mw3 = new addons::MessageWidget {mainWindow};
  mw3->setMessageType(addons::MessageWidget::MessageType::Warning);
  mw3->setWordWrap(true);
  mw3->setText(QStringLiteral("A KMessageWidget of Warning type."));

  auto* mw4 = new addons::MessageWidget {mainWindow};
  mw4->setMessageType(addons::MessageWidget::MessageType::Error);
  mw4->setWordWrap(true);
  mw4->setText(QStringLiteral("A KMessageWidget of Error type."));

  // A frame to materialize the end of the KMessageWidget
  auto* frame = new QFrame {mainWindow};
  frame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
  frame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  auto* wordWrapCb = new QCheckBox {QStringLiteral("wordWrap"), mainWindow};
  wordWrapCb->setChecked(true);

  QObject::connect(wordWrapCb,
                   &QAbstractButton::toggled,
                   mw,
                   &addons::MessageWidget::setWordWrap);
  QObject::connect(wordWrapCb,
                   &QAbstractButton::toggled,
                   mw2,
                   &addons::MessageWidget::setWordWrap);
  QObject::connect(wordWrapCb,
                   &QAbstractButton::toggled,
                   mw3,
                   &addons::MessageWidget::setWordWrap);
  QObject::connect(wordWrapCb,
                   &QAbstractButton::toggled,
                   mw4,
                   &addons::MessageWidget::setWordWrap);

  auto* closeButtonCb =
      new QCheckBox(QStringLiteral("closeButton"), mainWindow);
  closeButtonCb->setChecked(true);

  QObject::connect(closeButtonCb,
                   &QAbstractButton::toggled,
                   mw,
                   &addons::MessageWidget::setCloseButtonVisible);
  QObject::connect(closeButtonCb,
                   &QAbstractButton::toggled,
                   mw2,
                   &addons::MessageWidget::setCloseButtonVisible);
  QObject::connect(closeButtonCb,
                   &QAbstractButton::toggled,
                   mw3,
                   &addons::MessageWidget::setCloseButtonVisible);
  QObject::connect(closeButtonCb,
                   &QAbstractButton::toggled,
                   mw4,
                   &addons::MessageWidget::setCloseButtonVisible);

  l->addWidget(wordWrapCb);
  l->addWidget(closeButtonCb);
  l->addWidget(mw);
  l->addWidget(mw2);
  l->addWidget(mw3);
  l->addWidget(mw4);
  l->addWidget(frame);

  mainWindow->resize(400, 300);
  mainWindow->show();

  // demonstrate linkActivated
  Receiver* info = new Receiver(mw);
  QObject::connect(mw,
                   &addons::MessageWidget::linkActivated,
                   info,
                   &Receiver::showWhatsThis);

  return app.exec();
  delete mainWindow;
}

#include "message_widget_example.moc"
