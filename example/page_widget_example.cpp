#include <QApplication>
#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>

#include "page_widget.h"

#include "utils.h"

using namespace addons;

class PageWidgetTest : public QWidget
{
  Q_OBJECT

public:
  explicit PageWidgetTest(QWidget* parent = nullptr);
  ~PageWidgetTest() override;

private slots:
  void setAutoFace();
  void setPlainFace();
  void setListFace();
  void setTreeFace();
  void setTabbedFace();

  void addPage();
  void addSubPage();
  void insertPage();
  void deletePage();

  void currentPageChanged(addons::PageWidgetItem* current,
                          addons::PageWidgetItem* before) const;
  void pageToggled(addons::PageWidgetItem* item, bool checked);

private:
  std::unique_ptr<PageWidget> m_widget;
};

PageWidgetTest::PageWidgetTest(QWidget* parent)
    : QWidget {parent}
    , m_widget {std::make_unique<PageWidget>(this)}
{
  auto* layout {new QGridLayout {this}};
  layout->addWidget(m_widget.get(), 0, 0, 7, 1);

  connect(m_widget.get(),
          &PageWidget::currentPageChanged,
          this,
          &PageWidgetTest::currentPageChanged);
  connect(m_widget.get(),
          &PageWidget::pageToggled,
          this,
          &PageWidgetTest::pageToggled);

  auto row_count {0};

  auto* button {new QPushButton {QStringLiteral("Auto"), this}};
  layout->addWidget(button, row_count, 1);
  connect(
      button, &QAbstractButton::clicked, this, &PageWidgetTest::setAutoFace);
  row_count++;

  button = new QPushButton {QStringLiteral("Plain"), this};
  layout->addWidget(button, row_count, 1);
  connect(
      button, &QAbstractButton::clicked, this, &PageWidgetTest::setPlainFace);
  row_count++;

  button = new QPushButton {QStringLiteral("List"), this};
  layout->addWidget(button, row_count, 1);
  connect(
      button, &QAbstractButton::clicked, this, &PageWidgetTest::setListFace);
  row_count++;

  button = new QPushButton {QStringLiteral("Tree"), this};
  layout->addWidget(button, row_count, 1);
  connect(
      button, &QAbstractButton::clicked, this, &PageWidgetTest::setTreeFace);
  row_count++;

  button = new QPushButton {QStringLiteral("Tabbed"), this};
  layout->addWidget(button, row_count, 1);
  connect(
      button, &QAbstractButton::clicked, this, &PageWidgetTest::setTabbedFace);
  row_count++;

  button = new QPushButton {QStringLiteral("Add Page"), this};
  layout->addWidget(button, row_count, 1);
  connect(button, &QAbstractButton::clicked, this, &PageWidgetTest::addPage);
  row_count++;

  button = new QPushButton {QStringLiteral("Add Sub Page"), this};
  layout->addWidget(button, row_count, 1);
  connect(button, &QAbstractButton::clicked, this, &PageWidgetTest::addSubPage);
  row_count++;

  button = new QPushButton {QStringLiteral("Insert Page"), this};
  layout->addWidget(button, row_count, 1);
  connect(button, &QAbstractButton::clicked, this, &PageWidgetTest::insertPage);
  row_count++;

  button = new QPushButton {QStringLiteral("Delete Page"), this};
  layout->addWidget(button, row_count, 1);
  connect(button, &QAbstractButton::clicked, this, &PageWidgetTest::deletePage);
  row_count++;

  auto* item {m_widget->addPage(new QPushButton {QStringLiteral("folfer")},
                                QStringLiteral("folder"))};
  item->setIcon(QIcon::fromTheme(QStringLiteral("folder")));
  item->setHeader("TESTERT");

  item = m_widget->addSubPage(item,
                              new QPushButton {QStringLiteral("subfolder")},
                              QStringLiteral("subfolder"));
  item->setIcon(QIcon::fromTheme(QStringLiteral("folder")));

  item = m_widget->addPage(new QLabel {QStringLiteral("second folder")},
                           QStringLiteral("second folder"));
  item->setIcon(QIcon::fromTheme(QStringLiteral("folder")));
}

PageWidgetTest::~PageWidgetTest() = default;

void PageWidgetTest::setAutoFace()
{
  m_widget->setFaceType(PageWidget::FaceType::Auto);
}

void PageWidgetTest::setPlainFace()
{
  m_widget->setFaceType(PageWidget::FaceType::Plain);
}

void PageWidgetTest::setListFace()
{
  m_widget->setFaceType(PageWidget::FaceType::List);
}

void PageWidgetTest::setTreeFace()
{
  m_widget->setFaceType(PageWidget::FaceType::Tree);
}

void PageWidgetTest::setTabbedFace()
{
  m_widget->setFaceType(PageWidget::FaceType::Tabbed);
}

void PageWidgetTest::addPage()
{
  static auto counter {0};
  const auto title {QStringLiteral("dynamic folder %1").arg(counter)};

  auto* item {m_widget->addPage(new QPushButton {title}, title)};
  item->setIcon(QIcon::fromTheme(QStringLiteral("folder")));
  item->setHeader(QStringLiteral("Header Test No %1").arg(counter));
  item->setCheckable(true);

  counter++;
}

void PageWidgetTest::addSubPage()
{
  static auto counter {0};
  auto* item {m_widget->currentPage()};
  if (item == nullptr) {
    return;
  }

  const auto title {QStringLiteral("subfolder %1").arg(counter)};
  item = m_widget->addSubPage(item, new QLabel {title}, title);
  item->setIcon(QIcon::fromTheme(QStringLiteral("folder")));

  counter++;
}

void PageWidgetTest::insertPage()
{
  static auto counter {0};

  auto* item {m_widget->currentPage()};
  if (item == nullptr) {
    return;
  }

  const auto title {QStringLiteral("before folder %1").arg(counter)};
  item = m_widget->insertPage(item, new QLabel {title}, title);
  item->setIcon(QIcon::fromTheme(QStringLiteral("folder")));

  counter++;
}

void PageWidgetTest::deletePage()
{
  auto* item {m_widget->currentPage()};
  if (item != nullptr) {
    m_widget->removePage(item);
  }
}

void PageWidgetTest::currentPageChanged(PageWidgetItem* current,
                                        PageWidgetItem* before) const
{
  if (current != nullptr) {
    qDebug() << "Current item:" << current->name();
  } else {
    qDebug() << "No current item";
  }

  if (before != nullptr) {
    qDebug() << "Item before:" << before->name();
  } else {
    qDebug() << "No item before";
  }
}

void PageWidgetTest::pageToggled(PageWidgetItem* item, bool checked)
{
  qDebug() << "Item %s changed check state to" << item->name()
           << (checked ? "checked" : "unchecked");
}

auto main(int argc, char** argv) -> int
{
  QApplication::setApplicationName(QStringLiteral("PageWidgetDemoExample"));

  const QApplication app {argc, argv};

  PageWidgetTest test_widget;
  test_widget.show();

  return QApplication::exec();
}

// #include "moc_page_widget_example.cpp"
#include "page_widget_example.moc"
