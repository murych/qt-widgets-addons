/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2007 Urs Wolfer <uwolfer@kde.org>

 SPDX-License-Identifier: LGPL-2.0-only
*/

#include <QApplication>
#include <QCheckBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QWidget>

#include "title_widget.h"

class KTitleWidgetTestWidget : public QWidget
{
public:
  explicit KTitleWidgetTestWidget(QWidget* parent = nullptr)
      : QWidget(parent)
  {
    auto* mainLayout {new QVBoxLayout {this}};

    auto* titleWidget {new addons::TitleWidget {this}};
    titleWidget->setText(QStringLiteral("Title"));
    titleWidget->setIconSize(QSize(22, 22));
    titleWidget->setIcon(QIcon::fromTheme(QStringLiteral("kde")),
                         addons::TitleWidget::ImageAlignment::ImageLeft);

    mainLayout->addWidget(titleWidget);

    // -------------------------------------------------------------------------

    auto* errorTitle {new addons::TitleWidget {this}};
    errorTitle->setText(QStringLiteral("Title"));
    errorTitle->setComment(QStringLiteral("Error Comment"),
                           addons::TitleWidget::MessageType::ErrorMessage);
    //    errorTitle->setIconSize(QSize {22, 22});
    //    errorTitle->setIcon(addons::TitleWidget::MessageType::ErrorMessage,
    //                        addons::TitleWidget::ImageAlignment::ImageRight);

    mainLayout->addWidget(errorTitle);

    // -------------------------------------------------------------------------

    auto* checkboxTitleWidget {new addons::TitleWidget {this}};

    auto* checkBoxTitleMainWidget {new QWidget {this}};
    auto* titleLayout {new QVBoxLayout {checkBoxTitleMainWidget}};
    titleLayout->setContentsMargins(6, 6, 6, 6);

    auto* checkBox {new QCheckBox {QStringLiteral("Text Checkbox"),
                                   checkBoxTitleMainWidget}};
    titleLayout->addWidget(checkBox);
    checkboxTitleWidget->setWidget(checkBoxTitleMainWidget);

    mainLayout->addWidget(checkboxTitleWidget);

    // -------------------------------------------------------------------------

    auto* otherLabel {new QLabel {QStringLiteral("Some text..."), this}};
    mainLayout->addWidget(otherLabel);

    // -------------------------------------------------------------------------

    mainLayout->addStretch();
  }
};

int main(int argc, char** argv)
{
  QApplication::setApplicationName(QStringLiteral("ktitlewidgettest"));
  QApplication app {argc, argv};

  KTitleWidgetTestWidget mainWidget;
  mainWidget.show();

  return app.exec();
}
