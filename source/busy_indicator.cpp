/*
    SPDX-FileCopyrightText: 2019 Harald Sitter <sitter@kde.org>

 SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR
 LicenseRef-KDE-Accepted-LGPL
*/

#include <QApplication>
#include <QIcon>
#include <QPainter>
#include <QResizeEvent>
#include <QStyle>
#include <QVariantAnimation>

#include "busy_indicator.h"

namespace addons
{

class BusyIndicatorWidgetPrivate
{
public:
  BusyIndicatorWidgetPrivate(BusyIndicatorWidget* parent)
      : q {parent}
  {
    animation.setLoopCount(-1);
    animation.setDuration(2000);
    animation.setStartValue(0);
    animation.setEndValue(360);

    QObject::connect(&animation,
                     &QVariantAnimation::valueChanged,
                     q,
                     [=](auto value)
                     {
                       rotation = value.toReal();
                       q->update();
                     });
  }

  BusyIndicatorWidget* const q;
  QVariantAnimation animation;
  QIcon icon {QIcon::fromTheme(QStringLiteral("view-refresh"))};
  double rotation {0.0};
  QPointF paintCenter;
};

BusyIndicatorWidget::BusyIndicatorWidget(QWidget* parent)
    : QWidget {parent}
    , d {std::make_unique<BusyIndicatorWidgetPrivate>(this)}
{
}

BusyIndicatorWidget::~BusyIndicatorWidget() = default;

QSize BusyIndicatorWidget::minimumSizeHint() const
{
  const auto extent {
      QApplication::style()->pixelMetric(QStyle::PM_SmallIconSize)};
  return {extent, extent};
}

void BusyIndicatorWidget::showEvent(QShowEvent* event)
{
  QWidget::showEvent(event);
  d->animation.start();
}

void BusyIndicatorWidget::hideEvent(QHideEvent* event)
{
  QWidget::hideEvent(event);
  d->animation.stop();
}

void BusyIndicatorWidget::resizeEvent(QResizeEvent* event)
{
  QWidget::resizeEvent(event);
  d->paintCenter =
      QPointF {event->size().width() / 2.0, event->size().height() / 2.0};
}

void BusyIndicatorWidget::paintEvent(QPaintEvent*)
{
  QPainter painter {this};
  painter.setRenderHint(QPainter::SmoothPixmapTransform);

  painter.translate(d->paintCenter);
  painter.rotate(d->rotation);
  painter.translate(-d->paintCenter);

  d->icon.paint(&painter, rect());
}

bool BusyIndicatorWidget::event(QEvent* event)
{
  return QWidget::event(event);
}

}  // namespace addons

#include "moc_busy_indicator.cpp"
