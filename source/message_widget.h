/*
    This file is part of the KDE libraries

 SPDX-FileCopyrightText: 2011 Aurélien Gâteau <agateau@kde.org>
 SPDX-FileCopyrightText: 2014 Dominik Haumann <dhaumann@kde.org>

 SPDX-License-Identifier: LGPL-2.1-or-later
*/

#ifndef ADDONS_MESSAGEWIDGET_H
#define ADDONS_MESSAGEWIDGET_H

#include <QFrame>

#include <widgets-addons/widgets-addons_export.hpp>

namespace addons
{

class WIDGETS_ADDONS_EXPORT MessageWidget : public QFrame
{
  Q_OBJECT
  Q_PROPERTY(QString text READ text WRITE setText)
  Q_PROPERTY(bool wordWrap READ wordWrap WRITE setWordWrap)
  Q_PROPERTY(bool closeButtonVisible READ isCloseButtonVisible WRITE
                 setCloseButtonVisible)
  Q_PROPERTY(MessageType messageType READ messageType WRITE setMessageType)
  Q_PROPERTY(QIcon icon READ icon WRITE setIcon)

public:
  enum class MessageType
  {
    Positive,
    Information,
    Warning,
    Error
  };
  Q_ENUM(MessageType)

  explicit MessageWidget(QWidget* parent = nullptr);
  explicit MessageWidget(const QString& text, QWidget* parent = nullptr);
  ~MessageWidget() override;

  [[nodiscard]] auto text() const -> QString;
  [[nodiscard]] auto wordWrap() const -> bool;
  [[nodiscard]] auto isCloseButtonVisible() const -> bool;
  [[nodiscard]] auto messageType() const -> MessageType;

  void addAction(QAction* action);
  void removeAction(QAction* action);
  void clearActions();

  [[nodiscard]] auto sizeHint() const -> QSize override;
  [[nodiscard]] auto minimumSizeHint() const -> QSize override;

  [[nodiscard]] auto heightForWidth(int width) const -> int override;

  [[nodiscard]] auto icon() const -> QIcon;

  [[nodiscard]] auto isHideAnimationRunning() const -> bool;
  [[nodiscard]] auto isShowAnimationRunning() const -> bool;

protected:
  void paintEvent(QPaintEvent* event) override;
  void resizeEvent(QResizeEvent* event) override;

  [[nodiscard]] auto event(QEvent* event) -> bool override;

private:
  std::unique_ptr<struct MessageWidgetPrivate> const d;

signals:
  void linkActivated(const QString& contents);
  void linkHovered(const QString& contents);
  void hideAnimationFinished();
  void showAnimationFinished();

public slots:
  void setText(const QString& text);
  void setWordWrap(bool word_wrap);
  void setCloseButtonVisible(bool visible);
  void setMessageType(MessageWidget::MessageType type);

  void animatedShow();
  void animatedHide();

  void setIcon(const QIcon& icon);
};

}  // namespace addons

#endif  // ADDONS_MESSAGEWIDGET_H
