/*
    This file is part of the KDE project
    SPDX-FileCopyrightText: 2007 Matthias Kretz <kretz@kde.org>

 SPDX-License-Identifier: LGPL-2.0-only
*/

#ifndef PAGE_WIDGET_MODEL_P_H
#define PAGE_WIDGET_MODEL_P_H

#include "page_model_p.h"
#include "page_widget_model.h"

namespace addons
{

class PageItem
{
public:
  explicit PageItem(PageWidgetItem* pageItem, PageItem* parent = nullptr);
  ~PageItem();

  PageItem(const PageItem&) = delete;
  PageItem& operator=(const PageItem&) = delete;

  void appendChild(PageItem* child);
  void insertChild(int row, PageItem* child);
  void removeChild(int row);

  PageItem* child(int row);
  int childCount() const;
  int columnCount() const;
  int row() const;
  PageItem* parent();

  PageWidgetItem* pageWidgetItem() const;

  PageItem* findChild(const PageWidgetItem* item);

  void dump(int indent = 0);

private:
  PageWidgetItem* mPageWidgetItem;

  QList<PageItem*> mChildItems;
  PageItem* mParentItem;
};

class PageWidgetModelPrivate : public PageModelPrivate
{
  Q_DECLARE_PUBLIC(PageWidgetModel)
protected:
  PageWidgetModelPrivate()
      : rootItem(new PageItem(nullptr, nullptr))
  {
  }

  ~PageWidgetModelPrivate() override
  {
    delete rootItem;
    rootItem = nullptr;
  }

  PageItem* rootItem;

  void _k_itemChanged()
  {
    Q_Q(PageWidgetModel);
    PageWidgetItem* item = qobject_cast<PageWidgetItem*>(q->sender());
    if (!item) {
      return;
    }

    const QModelIndex index = q->index(item);
    if (!index.isValid()) {
      return;
    }

    Q_EMIT q->dataChanged(index, index);
  }

  void _k_itemToggled(bool checked)
  {
    Q_Q(PageWidgetModel);
    PageWidgetItem* item = qobject_cast<PageWidgetItem*>(q->sender());
    if (!item) {
      return;
    }

    Q_EMIT q->toggled(item, checked);
  }
};

}  // namespace addons

#endif  // PageWidgetModel_P_H
