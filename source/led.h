/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 1998 Jörg Habenicht <j.habenicht@europemail.com>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef ADDONS_LED_H
#define ADDONS_LED_H

#include <QWidget>

#include <widgets-addons/widgets-addons_export.hpp>

namespace addons
{

class WIDGETS_ADDONS_EXPORT Led : public QWidget
{
  Q_OBJECT
  Q_PROPERTY(State state READ state WRITE setState)
  Q_PROPERTY(Shape shape READ shape WRITE setShape)
  Q_PROPERTY(Look look READ look WRITE setLook)
  Q_PROPERTY(QColor color READ color WRITE setColor)
  Q_PROPERTY(int darkFactor READ darkFactor WRITE setDarkFactor)

public:
  enum State
  {
    Off,
    On
  };
  Q_ENUM(State)

  enum Shape
  {
    Rectangular,
    Circular
  };
  Q_ENUM(Shape)

  enum Look
  {
    Flat,
    Raised,
    Sunken
  };
  Q_ENUM(Look)

  explicit Led(QWidget* parent = nullptr);
  explicit Led(const QColor& color, QWidget* parent = nullptr);

  Led(const QColor& color,
      Led::State state,
      Led::Look look,
      Led::Shape shape,
      QWidget* parent = nullptr);
  ~Led() override;

  [[nodiscard]] auto color() const -> QColor;
  [[nodiscard]] auto state() const -> State;
  [[nodiscard]] auto look() const -> Look;
  [[nodiscard]] auto shape() const -> Shape;
  [[nodiscard]] auto darkFactor() const -> int;

  void setColor(const QColor& color);
  void setState(State state);
  void setLook(Look look);
  void setShape(Shape shape);
  void setDarkFactor(int dark_factor);

  [[nodiscard]] auto sizeHint() const -> QSize override;
  [[nodiscard]] auto minimumSizeHint() const -> QSize override;

protected:
  void paintEvent(QPaintEvent*) override;
  void resizeEvent(QResizeEvent*) override;

  void updateCachedPixmap();

private:
  void updateAccessibleName();
  std::unique_ptr<class LedPrivate> const d;

public slots:
  void toggle();
  void on();
  void off();
};

}  // namespace addons

#endif  // ADDONS_LED_H
