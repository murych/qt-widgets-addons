#ifndef WIDGETS_JOYPAD_H
#define WIDGETS_JOYPAD_H

#include <QWidget>
#include <memory>

#include <widgets-addons/widgets-addons_export.hpp>

class QPropertyAnimation;
class QParallelAnimationGroup;

namespace addons
{

class WIDGETS_ADDONS_EXPORT JoyPad : public QWidget
{
  Q_OBJECT
  Q_PROPERTY(float x READ x WRITE setX NOTIFY xChanged)
  Q_PROPERTY(float y READ y WRITE setY NOTIFY yChanged)
public:
  explicit JoyPad(QWidget* parent = nullptr);
  ~JoyPad() override;

  [[nodiscard]] auto x() const { return m_x; }
  [[nodiscard]] auto y() const { return m_y; }

signals:
  void xChanged(float value);
  void yChanged(float value);

public slots:
  void setX(float value);
  void setY(float value);

  // Add or remove the knob return animations in x or y- direction.
  void removeXAnimation();
  void addXAnimation();

  void removeYAnimation();
  void addYAnimation();

  /*  Set the alignment of the quadratic content if the widgets geometry isn
   * quadratic. Flags can be combined eg. setAlignment(Qt::AlignLeft |
   * Qt::AlignBottom);
   */
  void setAlignment(Qt::Alignment f);

private:
  void resizeEvent(QResizeEvent* event) override;
  void paintEvent(QPaintEvent* event) override;
  void mousePressEvent(QMouseEvent* event) override;
  void mouseReleaseEvent(QMouseEvent* event) override;
  void mouseMoveEvent(QMouseEvent* event) override;

  float m_x {0.F};
  float m_y {0.F};

  std::unique_ptr<QParallelAnimationGroup> m_returnAnimation {nullptr};
  std::unique_ptr<QPropertyAnimation> m_xAnimation {nullptr};
  std::unique_ptr<QPropertyAnimation> m_yAnimation {nullptr};

  QRectF m_bounds {};
  QRectF m_knopBounds {};

  QPoint m_lastPos {};
  bool knopPressed {false};

  Qt::Alignment m_alignment {Qt::AlignCenter};
};

}  // namespace addons

#endif
