/*
    This file is part of the KDE Libraries
    SPDX-FileCopyrightText: 2006 Tobias Koenig <tokoe@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef ADDONS_PAGEWIDGET_H
#define ADDONS_PAGEWIDGET_H

#include "page_view.h"
#include "page_widget_model.h"
#include "widgets-addons/widgets-addons_export.hpp"

namespace addons
{

class PageWidgetPrivate;
/**
 * @class PageWidget PageWidget.h PageWidget
 *
 * @short Page widget with many layouts (faces).
 * A PageView with hierarchical page model.
 *
 * @author Tobias Koenig (tokoe@kde.org)
 */
class WIDGETS_ADDONS_EXPORT PageWidget : public PageView
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(PageWidget)

public:
  /**
   * Creates a new page widget.
   *
   * @param parent The parent widget.
   */
  explicit PageWidget(QWidget* parent = nullptr);

  /**
   * Destroys the page widget.
   */
  ~PageWidget() override;

  /**
   * Adds a new top level page to the widget.
   *
   * @param widget The widget of the page.
   * @param name The name which is displayed in the navigation view.
   *
   * @returns The associated PageWidgetItem.
   */
  PageWidgetItem* addPage(QWidget* widget, const QString& name);

  /**
   * Adds a new top level page to the widget.
   *
   * @param item The PageWidgetItem which describes the page.
   */
  void addPage(PageWidgetItem* item);

  /**
   * Inserts a new page in the widget.
   *
   * @param before The new page will be insert before this PageWidgetItem
   *               on the same level in hierarchy.
   * @param widget The widget of the page.
   * @param name The name which is displayed in the navigation view.
   *
   * @returns The associated PageWidgetItem.
   */
  PageWidgetItem* insertPage(PageWidgetItem* before,
                             QWidget* widget,
                             const QString& name);

  /**
   * Inserts a new page in the widget.
   *
   * @param before The new page will be insert before this PageWidgetItem
   *               on the same level in hierarchy.
   *
   * @param item The PageWidgetItem which describes the page.
   */
  void insertPage(PageWidgetItem* before, PageWidgetItem* item);

  /**
   * Inserts a new sub page in the widget.
   *
   * @param parent The new page will be insert as child of this PageWidgetItem.
   * @param widget The widget of the page.
   * @param name The name which is displayed in the navigation view.
   *
   * @returns The associated PageWidgetItem.
   */
  PageWidgetItem* addSubPage(PageWidgetItem* parent,
                             QWidget* widget,
                             const QString& name);

  /**
   * Inserts a new sub page in the widget.
   *
   * @param parent The new page will be insert as child of this PageWidgetItem.
   *
   * @param item The PageWidgetItem which describes the page.
   */
  void addSubPage(PageWidgetItem* parent, PageWidgetItem* item);

  /**
   * Removes the page associated with the given PageWidgetItem.
   */
  void removePage(PageWidgetItem* item);

  /**
   * Sets the page which is associated with the given PageWidgetItem to
   * be the current page and emits the currentPageChanged() signal.
   */
  void setCurrentPage(PageWidgetItem* item);

  /**
   * Returns the PageWidgetItem for the current page or a null pointer if there
   * is no current page.
   */
  PageWidgetItem* currentPage() const;

Q_SIGNALS:
  /**
   * This signal is emitted whenever the current page has changed.
   *
   * @param current The new current page or a null pointer if no current page is
   * available.
   * @param before The page that was current before the new current page has
   * changed.
   */
  void currentPageChanged(PageWidgetItem* current, PageWidgetItem* before);

  /**
   * This signal is emitted whenever a checkable page changes its state. @param
   * checked is true when the @p page is checked, or false if the @p page is
   * unchecked.
   */
  void pageToggled(PageWidgetItem* page, bool checked);

  /**
   * This signal is emitted when a page is removed.
   * @param page The page which is removed
   * */
  void pageRemoved(PageWidgetItem* page);

protected:
  PageWidget(PageWidgetPrivate& dd, QWidget* parent);
};

}  // namespace addons

#endif  // ADDONS_PAGEWIDGET_H
