/*
    This file is part of the KDE Libraries
    SPDX-FileCopyrightText: 2006 Tobias Koenig <tokoe@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef MODELS_PAGEMODEL_H
#define MODELS_PAGEMODEL_H

#include <QAbstractItemModel>
#include <memory>

#include "widgets-addons/widgets-addons_export.hpp"

namespace addons
{
class PageModelPrivate;

/**
 *  @class PageModel PageModel.h PageModel
 *
 *  @short A base class for a model used by PageView.
 *
 *  This class is an abstract base class which must be used to
 *  implement custom models for PageView. Additional to the standard
 *  Qt::ItemDataRoles it provides the two roles
 *
 *    @li HeaderRole
 *    @li HeaderVisibleRole
 *    @li WidgetRole
 *
 *  which are used to return a header string for a page and a QWidget
 *  pointer to the page itself.
 *
 *  <b>Example:</b>\n
 *
 *  \code
 *    PageView *view = new PageView( this );
 *    PageModel *model = new MyPageModel( this );
 *
 *    view->setModel( model );
 *  \endcode
 *
 *  @see PageView
 *  @author Tobias Koenig <tokoe@kde.org>
 */
class WIDGETS_ADDONS_EXPORT PageModel : public QAbstractItemModel
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(PageModel)

public:
  /**
   * Additional roles that PageView uses.
   */
  enum Role
  {
    /**
     * A string to be rendered as page header.
     */
    HeaderRole = Qt::UserRole + 1,
    /**
     * A pointer to the page widget. This is the widget that is shown when the
     * item is selected.
     *
     * You can make QVariant take a QWidget using
     * \code
     * QWidget *myWidget = new QWidget;
     * QVariant v = QVariant::fromValue(myWidget);
     * \endcode
     */
    WidgetRole,
    /**
     * when true, show the page header, if false don't
     * @since 5.52
     */
    HeaderVisibleRole,
  };

  /**
   * Constructs a page model with the given parent.
   */
  explicit PageModel(QObject* parent = nullptr);

  /**
   * Destroys the page model.
   */
  ~PageModel() override;

protected:
  PageModel(PageModelPrivate& dd, QObject* parent);
  std::unique_ptr<class PageModelPrivate> const d_ptr;
};

}  // namespace addons

#endif
