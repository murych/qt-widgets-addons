/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2007-2009 Urs Wolfer <uwolfer@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef WIDGETS_TITLEWIDGET_H
#define WIDGETS_TITLEWIDGET_H

#include <QWidget>

#include <widgets-addons/widgets-addons_export.hpp>

namespace addons
{

class WIDGETS_ADDONS_EXPORT TitleWidget : public QWidget
{
  Q_OBJECT

public:
  enum class ImageAlignment
  {
    ImageLeft,
    ImageRight
  };
  Q_ENUM(ImageAlignment)

  enum class MessageType
  {
    PlainMessage,
    InfoMessage,
    WarningMessage,
    ErrorMessage
  };

  explicit TitleWidget(QWidget* parent = nullptr);
  ~TitleWidget() override;

  TitleWidget(const TitleWidget&) = delete;
  TitleWidget(TitleWidget&&) noexcept = delete;
  auto operator=(const TitleWidget&) -> TitleWidget& = delete;
  auto operator=(TitleWidget&&) noexcept -> TitleWidget& = delete;

  void setWidget(QWidget* widget);

  [[nodiscard]] auto text() const -> QString;
  [[nodiscard]] auto comment() const -> QString;

  [[nodiscard]] auto icon() const -> QIcon;
  [[nodiscard]] auto iconSize() const -> QSize;

  void setBuddy(QWidget* buddy);

  [[nodiscard]] auto autoHideTimeout() const -> int;

  [[nodiscard]] auto level() -> int;

protected:
  void changeEvent(QEvent* e) override;
  void showEvent(QShowEvent* event) override;

  [[nodiscard]] auto eventFilter(QObject* object, QEvent* event)
      -> bool override;

private:
  std::unique_ptr<struct TitleWidgetPrivate> const d {nullptr};

public slots:
  void setText(const QString& text,
               Qt::Alignment alignment = Qt::AlignLeft | Qt::AlignVCenter);
  void setText(const QString& text, TitleWidget::MessageType type);
  void setComment(const QString& comment,
                  MessageType type = MessageType::PlainMessage);
  void setIcon(const QIcon& icon,
               ImageAlignment alignment = ImageAlignment::ImageRight);
  void setIcon(MessageType type,
               ImageAlignment alignment = ImageAlignment::ImageRight);
  void setIconSize(const QSize& icon_size);
  void setAutoHideTimeout(int msecs);
  void setLevel(int level);
};

}  // namespace addons

#endif  // WIDGETS_TITLEWIDGET_H
