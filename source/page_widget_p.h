/*
    This file is part of the KDE project
    SPDX-FileCopyrightText: 2007 Matthias Kretz <kretz@kde.org>

 SPDX-License-Identifier: LGPL-2.0-only
*/

#ifndef PAGE_WIDGET_P_H
#define PAGE_WIDGET_P_H

#include "page_view_p.h"
#include "page_widget.h"

namespace addons
{

class PageWidgetModel;
class PageWidgetPrivate : public PageViewPrivate
{
  Q_DECLARE_PUBLIC(PageWidget)
protected:
  PageWidgetPrivate(PageWidget* qq);

  PageWidgetModel* model() const
  {
    return static_cast<PageWidgetModel*>(PageViewPrivate::model);
  }

  void slotCurrentPageChanged(const QModelIndex&, const QModelIndex&);
};

}  // namespace addons

#endif  // PAGE_WIDGET_P_H
