/*
 * This is nearly complete Material design Switch widget implementation in
 * qtwidgets module. More info:
 * https://material.io/design/components/selection-controls.html#switches
 * Copyright (C) 2018 Iman Ahmadvand
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#ifndef STYLE_H
#define STYLE_H

#include <QEasingCurve>
#include <QPainter>

// #define cyan500 QColor{"#00bcd4"}
// #define gray50 QColor{}
// #define black QColor{}
// #define gray400 QColor{}

const QColor cyan500 {"#00bcd4"};
const QColor gray50 {"#fafafa"};
const QColor black {"#000000"};
const QColor gray400 {"#bdbdbd"};

Q_DECL_IMPORT void qt_blurImage(
    QPainter* p,
    QImage& blurImage,
    qreal radius,
    bool quality,
    bool alphaOnly,
    int transposed = 0);  // src/widgets/effects/qpixmapfilter.cpp

namespace Style
{

using Type = QEasingCurve::Type;

struct Animation
{
  Animation() = default;
  Animation(Type _easing, int _duration)
      : easing {_easing}
      , duration {_duration}
  {
  }

  Type easing;
  int duration;
};

struct Switch
{
  Switch()
      // : height {36}
      // ,
      : font {"Roboto medium", 13}
      , indicatorMargin {8, 8, 8, 8}
      , thumbOnBrush {cyan500}
      , trackOnBrush {cyan500}
      , thumbOffBrush {gray50}
      , trackOffBrush {black}
      , thumbDisabled {gray400}
      , trackDisabled {black}
      , textColor {black}
      , thumbBrushAnimation {Type::Linear, 150}
      , trackBrushAnimation {Type::Linear, 150}
      , thumbPosAniamtion {Type::InOutQuad, 150}
  {
  }

  int height {36};
  QFont font;
  QMargins indicatorMargin;
  QColor thumbOnBrush;
  double thumbOnOpacity {1};
  QColor trackOnBrush;
  double trackOnOpacity {0.5};
  QColor thumbOffBrush;
  double thumbOffOpacity {1};
  QColor trackOffBrush;
  double trackOffOpacity {0.38};
  QColor thumbDisabled;
  double thumbDisabledOpacity {1};
  QColor trackDisabled;
  double trackDisabledOpacity {0.12};
  QColor textColor;
  double disabledTextOpacity {0.26};
  Animation thumbBrushAnimation;
  Animation trackBrushAnimation;
  Animation thumbPosAniamtion;
};

inline QPixmap drawShadowEllipse(qreal radius,
                                 qreal elevation,
                                 const QColor& color)
{
  QPixmap px {static_cast<int>(radius * 2), static_cast<int>(radius * 2)};
  px.fill(Qt::transparent);

  {  // draw ellipes
    QPainter p {&px};
    p.setBrush(color);
    p.setPen(Qt::NoPen);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.drawEllipse(QRectF(0, 0, px.size().width(), px.size().height()).center(),
                  radius - elevation,
                  radius - elevation);
  }

  QImage tmp {px.size(), QImage::Format_ARGB32_Premultiplied};
  tmp.setDevicePixelRatio(px.devicePixelRatioF());
  tmp.fill(0);
  QPainter tmpPainter {&tmp};
  tmpPainter.setCompositionMode(QPainter::CompositionMode_Source);
  tmpPainter.drawPixmap(QPointF(), px);
  tmpPainter.end();

  // blur the alpha channel
  QImage blurred {tmp.size(), QImage::Format_ARGB32_Premultiplied};
  blurred.setDevicePixelRatio(px.devicePixelRatioF());
  blurred.fill(0);
  {
    QPainter blurPainter {&blurred};
    qt_blurImage(&blurPainter, tmp, elevation * 4., true, false);
  }

  tmp = blurred;

  return QPixmap::fromImage(tmp);
}

}  // namespace Style

#endif  // STYLE_H
