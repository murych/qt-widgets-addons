/*
    This file is part of the KDE Libraries
    SPDX-FileCopyrightText: 2006 Tobias Koenig <tokoe@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "page_widget.h"

#include "page_widget_model.h"
#include "page_widget_p.h"

namespace addons
{

PageWidgetPrivate::PageWidgetPrivate(PageWidget* qq)
    : PageViewPrivate(qq)
{
}

void PageWidgetPrivate::slotCurrentPageChanged(const QModelIndex& current,
                                               const QModelIndex& before)
{
  PageWidgetItem* currentItem = nullptr;
  if (current.isValid()) {
    currentItem = model()->item(current);
  }

  PageWidgetItem* beforeItem = nullptr;
  if (before.isValid()) {
    beforeItem = model()->item(before);
  }

  Q_Q(PageWidget);
  Q_EMIT q->currentPageChanged(currentItem, beforeItem);
}

PageWidget::PageWidget(PageWidgetPrivate& dd, QWidget* parent)
    : PageView(dd, parent)
{
  Q_D(PageWidget);
  connect(this,
          &PageView::currentPageChanged,
          this,
          [d](const QModelIndex& current, const QModelIndex& before)
          { d->slotCurrentPageChanged(current, before); });

  if (!d->PageViewPrivate::model) {
    setModel(new PageWidgetModel(this));
  } else {
    Q_ASSERT(qobject_cast<PageWidgetModel*>(d->PageViewPrivate::model));
  }

  connect(
      d->model(), &PageWidgetModel::toggled, this, &PageWidget::pageToggled);
}

PageWidget::PageWidget(QWidget* parent)
    : PageWidget(*new PageWidgetPrivate(this), parent)
{
}

PageWidget::~PageWidget() {}

PageWidgetItem* PageWidget::addPage(QWidget* widget, const QString& name)
{
  Q_D(PageWidget);

  return d->model()->addPage(widget, name);
}

void PageWidget::addPage(PageWidgetItem* item)
{
  Q_D(PageWidget);

  d->model()->addPage(item);
}

PageWidgetItem* PageWidget::insertPage(PageWidgetItem* before,
                                       QWidget* widget,
                                       const QString& name)
{
  Q_D(PageWidget);

  return d->model()->insertPage(before, widget, name);
}

void PageWidget::insertPage(PageWidgetItem* before, PageWidgetItem* item)
{
  Q_D(PageWidget);

  d->model()->insertPage(before, item);
}

PageWidgetItem* PageWidget::addSubPage(PageWidgetItem* parent,
                                       QWidget* widget,
                                       const QString& name)
{
  Q_D(PageWidget);

  return d->model()->addSubPage(parent, widget, name);
}

void PageWidget::addSubPage(PageWidgetItem* parent, PageWidgetItem* item)
{
  Q_D(PageWidget);

  d->model()->addSubPage(parent, item);
}

void PageWidget::removePage(PageWidgetItem* item)
{
  Q_D(PageWidget);

  Q_EMIT pageRemoved(item);  // Q_EMIT signal before we remove it, because the
                             // item will be deleted in the model
  d->model()->removePage(item);
}

void PageWidget::setCurrentPage(PageWidgetItem* item)
{
  Q_D(PageWidget);

  const QModelIndex index = d->model()->index(item);
  if (!index.isValid()) {
    return;
  }

  PageView::setCurrentPage(index);
}

PageWidgetItem* PageWidget::currentPage() const
{
  Q_D(const PageWidget);

  const QModelIndex index = PageView::currentPage();

  if (!index.isValid()) {
    return nullptr;
  }

  return d->model()->item(index);
}

}  // namespace addons

#include "moc_page_widget.cpp"
