/*
 * This is nearly complete Material design Switch widget implementation in
 * qtwidgets module. More info:
 * https://material.io/design/components/selection-controls.html#switches
 * Copyright (C) 2018-2020 Iman Ahmadvand
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

#include <QApplication>
#include <QDebug>

#include "switch.h"

namespace addons
{

Animator::Animator(QObject* target, QObject* parent)
    : QVariantAnimation {parent}
{
  setTargetObject(target);
}

Animator::~Animator()
{
  stop();
}

QObject* Animator::targetObject() const
{
  return target;
}

void Animator::setTargetObject(QObject* _target)
{
  if (target == _target) {
    return;
  }

  if (isRunning()) {
    qWarning() << "Animation::setTargetObject: you can't change the target of "
                  "a running animation";
    return;
  }

  delete target;
  target = _target;
}

void Animator::updateCurrentValue(const QVariant& value)
{
  Q_UNUSED(value);

  if (target != nullptr) {
    QEvent update {QEvent::StyleAnimationUpdate};
    update.setAccepted(false);
    QApplication::sendEvent(target, &update);
    if (!update.isAccepted()) {
      stop();
    }
  }
}

void Animator::updateState(QAbstractAnimation::State newState,
                           QAbstractAnimation::State oldState)
{
  if (target == nullptr && oldState == Stopped) {
    qWarning() << "Animation::updateState: Changing state of an animation "
                  "without target";
    return;
  }

  QVariantAnimation::updateState(newState, oldState);

  if (!endValue().isValid() && direction() == Forward) {
    qWarning() << "Animation::updateState"
               << targetObject()->metaObject()->className()
               << "starting an animation without end value";
  }
}

void Animator::setup(int duration, QEasingCurve easing)
{
  setDuration(duration);
  setEasingCurve(easing);
}

void Animator::interpolate(const QVariant& _start, const QVariant& end)
{
  setStartValue(_start);
  setEndValue(end);
  start();
}

void Animator::setCurrentValue(const QVariant& value)
{
  setStartValue(value);
  setEndValue(value);
  updateCurrentValue(currentValue());
}

SelectionControl::SelectionControl(QWidget* parent)
    : QAbstractButton {parent}
{
  setObjectName("SelectionControl");
  setCheckable(true);
}

SelectionControl::~SelectionControl() = default;

void SelectionControl::enterEvent(QEvent* e)
{
  setCursor(Qt::PointingHandCursor);
  QAbstractButton::enterEvent(e);
}

Qt::CheckState SelectionControl::checkState() const
{
  return isChecked() ? Qt::Checked : Qt::Unchecked;
}

void SelectionControl::checkStateSet()
{
  const auto state {checkState()};
  emit stateChanged(state);
  toggle(state);
}

void SelectionControl::nextCheckState()
{
  QAbstractButton::nextCheckState();
  SelectionControl::checkStateSet();
}

void Switch::init()
{
  setFont(style.font);
  setObjectName("Switch");
  /* setup animations */
  thumbBrushAnimation = new Animator {this, this};
  trackBrushAnimation = new Animator {this, this};
  thumbPosAniamtion = new Animator {this, this};
  thumbPosAniamtion->setup(style.thumbPosAniamtion.duration,
                           style.thumbPosAniamtion.easing);
  trackBrushAnimation->setup(style.trackBrushAnimation.duration,
                             style.trackBrushAnimation.easing);
  thumbBrushAnimation->setup(style.thumbBrushAnimation.duration,
                             style.thumbBrushAnimation.easing);
  /* set init values */
  trackBrushAnimation->setStartValue(
      colorFromOpacity(style.trackOffBrush, style.trackOffOpacity));
  trackBrushAnimation->setEndValue(
      colorFromOpacity(style.trackOffBrush, style.trackOffOpacity));
  thumbBrushAnimation->setStartValue(
      colorFromOpacity(style.thumbOffBrush, style.thumbOffOpacity));
  thumbBrushAnimation->setEndValue(
      colorFromOpacity(style.thumbOffBrush, style.thumbOffOpacity));
  /* set standard palettes */
  auto _palette {palette()};
  _palette.setColor(QPalette::Active, QPalette::ButtonText, style.textColor);
  _palette.setColor(QPalette::Disabled, QPalette::ButtonText, style.textColor);
  setPalette(_palette);
  setSizePolicy(
      QSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Fixed));
}

QRect Switch::indicatorRect()
{
  const auto _width {style.indicatorMargin.left() + style.height
                     + style.indicatorMargin.right()};
  return ltr(this) ? QRect {0, 0, _width, style.height}
                   : QRect {width() - _width, 0, _width, style.height};
}

QRect Switch::textRect()
{
  const auto _width {style.indicatorMargin.left() + style.height
                     + style.indicatorMargin.right()};
  return ltr(this) ? rect().marginsRemoved(QMargins {_width, 0, 0, 0})
                   : rect().marginsRemoved(QMargins {0, 0, _width, 0});
}

Switch::Switch(QWidget* parent)
    : SelectionControl {parent}
{
  init();
}

Switch::Switch(const QString& text, QWidget* parent)
    : Switch {parent}
{
  setText(text);
}

Switch::Switch(const QString& text, const QBrush& brush, QWidget* parent)
    : Switch {text, parent}
{
  style.thumbOnBrush = brush.color();
  style.trackOnBrush = brush.color();
}

Switch::~Switch() = default;

QSize Switch::sizeHint() const
{
  auto _height {style.height};
  auto _width {style.indicatorMargin.left() + style.height
               + style.indicatorMargin.right() + fontMetrics().width(text())};

  return {_width, _height};
}

void Switch::paintEvent(QPaintEvent* e)
{
  /* for desktop usage we do not need Radial reaction */
  QPainter painter {this};

  auto const _indicatorRect {indicatorRect()};
  auto const _textRect {textRect()};
  auto trackMargin {style.indicatorMargin};
  trackMargin.setTop(trackMargin.top() + 2);
  trackMargin.setBottom(trackMargin.bottom() + 2);
  QRectF trackRect {_indicatorRect.marginsRemoved(trackMargin)};

  if (isEnabled()) {
    painter.setOpacity(1.0);
    painter.setPen(Qt::NoPen);
    /* draw track */
    painter.setBrush(trackBrushAnimation->currentValue().value<QColor>());
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawRoundedRect(trackRect, CORNER_RADIUS, CORNER_RADIUS);
    painter.setRenderHint(QPainter::Antialiasing, false);
    /* draw thumb */
    trackRect.setX(trackRect.x() - trackMargin.left() - trackMargin.right() - 2
                   + thumbPosAniamtion->currentValue().toInt());
    auto thumbRect {trackRect};

    if (!shadowPixmap.isNull()) {
      painter.drawPixmap(
          thumbRect.center() - QPointF {THUMB_RADIUS, THUMB_RADIUS - 1.0},
          shadowPixmap);
    }

    painter.setBrush(thumbBrushAnimation->currentValue().value<QColor>());
    painter.setRenderHint(QPainter::Antialiasing, true);
    //        qDebug() << thumbRect << thumbPosAniamtion->currentValue();
    painter.drawEllipse(thumbRect.center(),
                        THUMB_RADIUS - SHADOW_ELEVATION - 1.0,
                        THUMB_RADIUS - SHADOW_ELEVATION - 1.0);
    painter.setRenderHint(QPainter::Antialiasing, false);

    /* draw text */
    if (text().isEmpty()) {
      return;
    }

    painter.setOpacity(1.0);
    painter.setPen(palette().color(QPalette::Active, QPalette::ButtonText));
    painter.setFont(font());
    painter.drawText(_textRect, Qt::AlignLeft | Qt::AlignVCenter, text());
  } else {
    painter.setOpacity(style.trackDisabledOpacity);
    painter.setPen(Qt::NoPen);
    // draw track
    painter.setBrush(style.trackDisabled);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawRoundedRect(trackRect, CORNER_RADIUS, CORNER_RADIUS);
    painter.setRenderHint(QPainter::Antialiasing, false);
    // draw thumb
    painter.setOpacity(1.0);
    if (!isChecked()) {
      trackRect.setX(trackRect.x() - trackMargin.left() - trackMargin.right()
                     - 2);
    } else {
      trackRect.setX(trackRect.x() + trackMargin.left() + trackMargin.right()
                     + 2);
    }
    auto thumbRect {trackRect};

    if (!shadowPixmap.isNull()) {
      painter.drawPixmap(
          thumbRect.center() - QPointF(THUMB_RADIUS, THUMB_RADIUS - 1.0),
          shadowPixmap);
    }

    painter.setOpacity(1.0);
    painter.setBrush(style.thumbDisabled);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawEllipse(thumbRect.center(),
                        THUMB_RADIUS - SHADOW_ELEVATION - 1.0,
                        THUMB_RADIUS - SHADOW_ELEVATION - 1.0);

    /* draw text */
    if (text().isEmpty()) {
      return;
    }

    painter.setOpacity(style.disabledTextOpacity);
    painter.setPen(palette().color(QPalette::Disabled, QPalette::ButtonText));
    painter.setFont(font());
    painter.drawText(_textRect, Qt::AlignLeft | Qt::AlignVCenter, text());
  }
}

void Switch::resizeEvent(QResizeEvent* e)
{
  shadowPixmap = Style::drawShadowEllipse(
      THUMB_RADIUS, SHADOW_ELEVATION, QColor {0, 0, 0, 70});
  SelectionControl::resizeEvent(e);
}

void Switch::toggle(Qt::CheckState state)
{
  if (state == Qt::Checked) {
    const QVariant posEnd {
        (style.indicatorMargin.left() + style.indicatorMargin.right() + 2) * 2};
    const QVariant thumbEnd {
        colorFromOpacity(style.thumbOnBrush, style.thumbOnOpacity)};
    const QVariant trackEnd {
        colorFromOpacity(style.trackOnBrush, style.trackOnOpacity)};

    if (!isVisible()) {
      thumbPosAniamtion->setCurrentValue(posEnd);
      thumbBrushAnimation->setCurrentValue(thumbEnd);
      trackBrushAnimation->setCurrentValue(trackEnd);
    } else {
      thumbPosAniamtion->interpolate(0, posEnd);
      thumbBrushAnimation->interpolate(
          colorFromOpacity(style.thumbOffBrush, style.thumbOffOpacity),
          thumbEnd);
      trackBrushAnimation->interpolate(
          colorFromOpacity(style.trackOffBrush, style.trackOffOpacity),
          trackEnd);
    }
  } else {  // Qt::Unchecked
    const QVariant posEnd {0};
    const QVariant thumbEnd {
        colorFromOpacity(style.thumbOffBrush, style.thumbOffOpacity)};
    const QVariant trackEnd {
        colorFromOpacity(style.trackOffBrush, style.trackOffOpacity)};

    if (!isVisible()) {
      thumbPosAniamtion->setCurrentValue(posEnd);
      thumbBrushAnimation->setCurrentValue(thumbEnd);
      trackBrushAnimation->setCurrentValue(trackEnd);
    } else {
      thumbPosAniamtion->interpolate(thumbPosAniamtion->currentValue().toInt(),
                                     posEnd);
      thumbBrushAnimation->interpolate(
          colorFromOpacity(style.thumbOnBrush, style.thumbOnOpacity), thumbEnd);
      trackBrushAnimation->interpolate(
          colorFromOpacity(style.trackOnBrush, style.trackOnOpacity), trackEnd);
    }
  }
}

}  // namespace addons
