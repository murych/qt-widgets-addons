/*
    SPDX-FileCopyrightText: 2019 Harald Sitter <sitter@kde.org>

 SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR
 LicenseRef-KDE-Accepted-LGPL
*/

#ifndef ADDONS_BUSYINDICATOR_H
#define ADDONS_BUSYINDICATOR_H

#include <QWidget>

#include <widgets-addons/widgets-addons_export.hpp>

namespace addons
{

class WIDGETS_ADDONS_EXPORT BusyIndicatorWidget : public QWidget
{
  Q_OBJECT
public:
  explicit BusyIndicatorWidget(QWidget* parent = nullptr);
  ~BusyIndicatorWidget() override;

  [[nodiscard]] auto minimumSizeHint() const -> QSize override;

protected:
  void showEvent(QShowEvent* event) override;
  void hideEvent(QHideEvent* event) override;
  void resizeEvent(QResizeEvent* event) override;
  void paintEvent(QPaintEvent*) override;

  auto event(QEvent* event) -> bool override;

private:
  std::unique_ptr<class BusyIndicatorWidgetPrivate> const d;
};

}  // namespace addons

#endif  // ADDONS_BUSYINDICATOR_H
