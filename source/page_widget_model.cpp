/*
    This file is part of the KDE Libraries
    SPDX-FileCopyrightText: 2006 Tobias Koenig <tokoe@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include <QIcon>
#include <QPointer>
#include <QWidget>

#include "page_widget_model.h"

#include "page_widget_model_p.h"

namespace addons
{

class PageWidgetItemPrivate
{
public:
  PageWidgetItemPrivate()
      : checkable(false)
      , checked(false)
      , enabled(true)
      , headerVisible(true)
  {
  }

  ~PageWidgetItemPrivate()
  {
    delete widget;
    widget = nullptr;
  }

  QString name;
  QString header;
  QIcon icon;
  QPointer<QWidget> widget;
  bool checkable : 1;
  bool checked : 1;
  bool enabled : 1;
  bool headerVisible : 1;
};

PageWidgetItem::PageWidgetItem(QWidget* widget)
    : QObject(nullptr)
    , d(new PageWidgetItemPrivate)
{
  d->widget = widget;

  // Hide the widget, otherwise when the widget has this KPageView as
  // parent the widget is shown outside the QStackedWidget if the page
  // was not selected ( and reparented ) yet.
  if (d->widget) {
    d->widget->hide();
  }
}

PageWidgetItem::PageWidgetItem(QWidget* widget, const QString& name)
    : QObject(nullptr)
    , d(new PageWidgetItemPrivate)
{
  d->widget = widget;
  d->name = name;

  // Hide the widget, otherwise when the widget has this KPageView as
  // parent the widget is shown outside the QStackedWidget if the page
  // was not selected ( and reparented ) yet.
  if (d->widget) {
    d->widget->hide();
  }
}

PageWidgetItem::~PageWidgetItem() = default;

void PageWidgetItem::setEnabled(bool enabled)
{
  d->enabled = enabled;
  if (d->widget) {
    d->widget->setEnabled(enabled);
  }
  Q_EMIT changed();
}

bool PageWidgetItem::isEnabled() const
{
  return d->enabled;
}

bool PageWidgetItem::isHeaderVisible() const
{
  return d->headerVisible;
}

void PageWidgetItem::setHeaderVisible(bool visible)
{
  d->headerVisible = visible;

  Q_EMIT changed();
}

QWidget* PageWidgetItem::widget() const
{
  return d->widget;
}

void PageWidgetItem::setName(const QString& name)
{
  d->name = name;

  Q_EMIT changed();
}

QString PageWidgetItem::name() const
{
  return d->name;
}

void PageWidgetItem::setHeader(const QString& header)
{
  const bool autoHeaderInvisibilityTriggered =
      header.isEmpty() & !header.isNull();
  //  if (autoHeaderInvisibilityTriggered) {
  //    qCWarning(KWidgetsAddonsLog)
  //        << "PageWidgetItem::setHeader() called with empty non-null string, "
  //           "which is deprecated. Use PageWidgetItem::setHeaderVisible(false)
  //           " "instead.";
  //  }

  d->header = header;

  Q_EMIT changed();
}

QString PageWidgetItem::header() const
{
  return d->header;
}

void PageWidgetItem::setIcon(const QIcon& icon)
{
  d->icon = icon;

  Q_EMIT changed();
}

QIcon PageWidgetItem::icon() const
{
  return d->icon;
}

void PageWidgetItem::setCheckable(bool checkable)
{
  d->checkable = checkable;

  Q_EMIT changed();
}

bool PageWidgetItem::isCheckable() const
{
  return d->checkable;
}

void PageWidgetItem::setChecked(bool checked)
{
  d->checked = checked;

  Q_EMIT toggled(checked);
  Q_EMIT changed();
}

bool PageWidgetItem::isChecked() const
{
  return d->checked;
}

PageItem::PageItem(PageWidgetItem* pageWidgetItem, PageItem* parent)
    : mPageWidgetItem(pageWidgetItem)
    , mParentItem(parent)
{
}

PageItem::~PageItem()
{
  delete mPageWidgetItem;
  mPageWidgetItem = nullptr;

  qDeleteAll(mChildItems);
}

void PageItem::appendChild(PageItem* item)
{
  mChildItems.append(item);
}

void PageItem::insertChild(int row, PageItem* item)
{
  mChildItems.insert(row, item);
}

void PageItem::removeChild(int row)
{
  mChildItems.removeAt(row);
}

PageItem* PageItem::child(int row)
{
  return mChildItems.value(row);
}

int PageItem::childCount() const
{
  return mChildItems.count();
}

int PageItem::columnCount() const
{
  return 1;
}

PageItem* PageItem::parent()
{
  return mParentItem;
}

int PageItem::row() const
{
  if (mParentItem) {
    return mParentItem->mChildItems.indexOf(const_cast<PageItem*>(this));
  }

  return 0;
}

PageWidgetItem* PageItem::pageWidgetItem() const
{
  return mPageWidgetItem;
}

PageItem* PageItem::findChild(const PageWidgetItem* item)
{
  if (mPageWidgetItem == item) {
    return this;
  }

  for (int i = 0; i < mChildItems.count(); ++i) {
    PageItem* pageItem = mChildItems[i]->findChild(item);
    if (pageItem) {
      return pageItem;
    }
  }

  return nullptr;
}

void PageItem::dump(int indent)
{
  const QString indentation(indent, QLatin1Char(' '));

  const QString name =
      (mPageWidgetItem ? mPageWidgetItem->name() : QStringLiteral("root"));
  //  qCDebug(KWidgetsAddonsLog,
  //          "%s (%p)",
  //          qPrintable(QString(indentation + name)),
  //          (void*)this);
  for (int i = 0; i < mChildItems.count(); ++i) {
    mChildItems[i]->dump(indent + 2);
  }
}

PageWidgetModel::PageWidgetModel(QObject* parent)
    : PageModel(*new PageWidgetModelPrivate, parent)
{
}

PageWidgetModel::~PageWidgetModel() {}

int PageWidgetModel::columnCount(const QModelIndex&) const
{
  return 1;
}

QVariant PageWidgetModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return QVariant();
  }

  PageItem* item = static_cast<PageItem*>(index.internalPointer());

  if (role == Qt::DisplayRole) {
    return QVariant(item->pageWidgetItem()->name());
  } else if (role == Qt::DecorationRole) {
    return QVariant(item->pageWidgetItem()->icon());
  } else if (role == HeaderRole) {
    return QVariant(item->pageWidgetItem()->header());
  } else if (role == HeaderVisibleRole) {
    return item->pageWidgetItem()->isHeaderVisible();
  } else if (role == WidgetRole) {
    return QVariant::fromValue(item->pageWidgetItem()->widget());
  } else if (role == Qt::CheckStateRole) {
    if (item->pageWidgetItem()->isCheckable()) {
      return (item->pageWidgetItem()->isChecked() ? Qt::Checked
                                                  : Qt::Unchecked);
    } else {
      return QVariant();
    }
  } else {
    return QVariant();
  }
}

bool PageWidgetModel::setData(const QModelIndex& index,
                              const QVariant& value,
                              int role)
{
  if (!index.isValid()) {
    return false;
  }

  if (role != Qt::CheckStateRole) {
    return false;
  }

  PageItem* item = static_cast<PageItem*>(index.internalPointer());
  if (!item) {
    return false;
  }

  if (!item->pageWidgetItem()->isCheckable()) {
    return false;
  }

  if (value.toInt() == Qt::Checked) {
    item->pageWidgetItem()->setChecked(true);
  } else {
    item->pageWidgetItem()->setChecked(false);
  }

  return true;
}

Qt::ItemFlags PageWidgetModel::flags(const QModelIndex& index) const
{
  if (!index.isValid()) {
    return Qt::NoItemFlags;
  }

  Qt::ItemFlags flags = Qt::ItemIsSelectable;

  PageItem* item = static_cast<PageItem*>(index.internalPointer());
  if (item->pageWidgetItem()->isCheckable()) {
    flags |= Qt::ItemIsUserCheckable;
  }
  if (item->pageWidgetItem()->isEnabled()) {
    flags |= Qt::ItemIsEnabled;
  }

  return flags;
}

QModelIndex PageWidgetModel::index(int row,
                                   int column,
                                   const QModelIndex& parent) const
{
  Q_D(const PageWidgetModel);

  PageItem* parentItem;

  if (parent.isValid()) {
    parentItem = static_cast<PageItem*>(parent.internalPointer());
  } else {
    parentItem = d->rootItem;
  }

  PageItem* childItem = parentItem->child(row);
  if (childItem) {
    return createIndex(row, column, childItem);
  } else {
    return QModelIndex();
  }
}

QModelIndex PageWidgetModel::parent(const QModelIndex& index) const
{
  Q_D(const PageWidgetModel);

  if (!index.isValid()) {
    return QModelIndex();
  }

  PageItem* item = static_cast<PageItem*>(index.internalPointer());
  PageItem* parentItem = item->parent();

  if (parentItem == d->rootItem) {
    return QModelIndex();
  } else {
    return createIndex(parentItem->row(), 0, parentItem);
  }
}

int PageWidgetModel::rowCount(const QModelIndex& parent) const
{
  Q_D(const PageWidgetModel);

  PageItem* parentItem;

  if (!parent.isValid()) {
    parentItem = d->rootItem;
  } else {
    parentItem = static_cast<PageItem*>(parent.internalPointer());
  }

  return parentItem->childCount();
}

PageWidgetItem* PageWidgetModel::addPage(QWidget* widget, const QString& name)
{
  PageWidgetItem* item = new PageWidgetItem(widget, name);

  addPage(item);

  return item;
}

void PageWidgetModel::addPage(PageWidgetItem* item)
{
  Q_EMIT layoutAboutToBeChanged();

  Q_D(PageWidgetModel);
  connect(item, SIGNAL(changed()), this, SLOT(_k_itemChanged()));
  connect(item, SIGNAL(toggled(bool)), this, SLOT(_k_itemToggled(bool)));

  // The row to be inserted
  int row = d->rootItem->childCount();

  beginInsertRows(QModelIndex(), row, row);

  PageItem* pageItem = new PageItem(item, d->rootItem);
  d->rootItem->appendChild(pageItem);

  endInsertRows();

  Q_EMIT layoutChanged();
}

PageWidgetItem* PageWidgetModel::insertPage(PageWidgetItem* before,
                                            QWidget* widget,
                                            const QString& name)
{
  PageWidgetItem* item = new PageWidgetItem(widget, name);

  insertPage(before, item);

  return item;
}

void PageWidgetModel::insertPage(PageWidgetItem* before, PageWidgetItem* item)
{
  Q_D(PageWidgetModel);

  PageItem* beforePageItem = d->rootItem->findChild(before);
  if (!beforePageItem) {
    //    qCDebug(KWidgetsAddonsLog, "Invalid PageWidgetItem passed!");
    return;
  }

  Q_EMIT layoutAboutToBeChanged();

  connect(item, SIGNAL(changed()), this, SLOT(_k_itemChanged()));
  connect(item, SIGNAL(toggled(bool)), this, SLOT(_k_itemToggled(bool)));

  PageItem* parent = beforePageItem->parent();
  // The row to be inserted
  int row = beforePageItem->row();

  QModelIndex index;
  if (parent != d->rootItem) {
    index = createIndex(parent->row(), 0, parent);
  }

  beginInsertRows(index, row, row);

  PageItem* newPageItem = new PageItem(item, parent);
  parent->insertChild(row, newPageItem);

  endInsertRows();

  Q_EMIT layoutChanged();
}

PageWidgetItem* PageWidgetModel::addSubPage(PageWidgetItem* parent,
                                            QWidget* widget,
                                            const QString& name)
{
  PageWidgetItem* item = new PageWidgetItem(widget, name);

  addSubPage(parent, item);

  return item;
}

void PageWidgetModel::addSubPage(PageWidgetItem* parent, PageWidgetItem* item)
{
  Q_D(PageWidgetModel);

  PageItem* parentPageItem = d->rootItem->findChild(parent);
  if (!parentPageItem) {
    //    qCDebug(KWidgetsAddonsLog, "Invalid PageWidgetItem passed!");
    return;
  }

  Q_EMIT layoutAboutToBeChanged();

  connect(item, SIGNAL(changed()), this, SLOT(_k_itemChanged()));
  connect(item, SIGNAL(toggled(bool)), this, SLOT(_k_itemToggled(bool)));

  // The row to be inserted
  int row = parentPageItem->childCount();

  QModelIndex index;
  if (parentPageItem != d->rootItem) {
    index = createIndex(parentPageItem->row(), 0, parentPageItem);
  }

  beginInsertRows(index, row, row);

  PageItem* newPageItem = new PageItem(item, parentPageItem);
  parentPageItem->appendChild(newPageItem);

  endInsertRows();

  Q_EMIT layoutChanged();
}

void PageWidgetModel::removePage(PageWidgetItem* item)
{
  if (!item) {
    return;
  }

  Q_D(PageWidgetModel);

  PageItem* pageItem = d->rootItem->findChild(item);
  if (!pageItem) {
    //    qCDebug(KWidgetsAddonsLog, "Invalid PageWidgetItem passed!");
    return;
  }

  Q_EMIT layoutAboutToBeChanged();

  disconnect(item, SIGNAL(changed()), this, SLOT(_k_itemChanged()));
  disconnect(item, SIGNAL(toggled(bool)), this, SLOT(_k_itemToggled(bool)));

  PageItem* parentPageItem = pageItem->parent();
  int row = parentPageItem->row();

  QModelIndex index;
  if (parentPageItem != d->rootItem) {
    index = createIndex(row, 0, parentPageItem);
  }

  beginRemoveRows(index, pageItem->row(), pageItem->row());

  parentPageItem->removeChild(pageItem->row());
  delete pageItem;

  endRemoveRows();

  Q_EMIT layoutChanged();
}

PageWidgetItem* PageWidgetModel::item(const QModelIndex& index) const
{
  if (!index.isValid()) {
    return nullptr;
  }

  PageItem* item = static_cast<PageItem*>(index.internalPointer());
  if (!item) {
    return nullptr;
  }

  return item->pageWidgetItem();
}

QModelIndex PageWidgetModel::index(const PageWidgetItem* item) const
{
  Q_D(const PageWidgetModel);

  if (!item) {
    return QModelIndex();
  }

  const PageItem* pageItem = d->rootItem->findChild(item);
  if (!pageItem) {
    return QModelIndex();
  }

  return createIndex(pageItem->row(), 0, (void*)pageItem);
}

}  // namespace addons

#include "moc_page_widget_model.cpp"
