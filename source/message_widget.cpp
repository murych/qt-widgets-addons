/*
    This file is part of the KDE libraries

    SPDX-FileCopyrightText: 2011 Aurélien Gâteau <agateau@kde.org>
    SPDX-FileCopyrightText: 2014 Dominik Haumann <dhaumann@kde.org>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <QAction>
#include <QApplication>
#include <QEvent>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPainter>
#include <QShowEvent>
#include <QStyle>
#include <QStyleOption>
#include <QTimeLine>
#include <QToolButton>

#include "message_widget.h"

namespace
{
constexpr auto border_size = 2;
}  // namespace

namespace addons
{

struct MessageWidgetPrivate
{
  void init(MessageWidget* q_ptr);

  MessageWidget* q {nullptr};
  QLabel* iconLabel {nullptr};
  QLabel* textLabel {nullptr};
  QToolButton* closeButton {nullptr};
  QTimeLine* timeLine {nullptr};
  MessageWidget::MessageType messageType {
      MessageWidget::MessageType::Information};
  QIcon icon;
  std::vector<QToolButton*> buttons;
  bool ignoreShowAndResizeEventDoingAnimatedShow {false};
  bool wordWrap {false};

  void createLayout();
  void updateLayout();
  void setPalette() const;
  void slotTimeLineChanged(double value) const;
  void slotTimeLineFinished() const;
  [[nodiscard]] auto bestContentHeight() const -> int;
};

void MessageWidgetPrivate::init(MessageWidget* q_ptr)
{
  q = q_ptr;
  timeLine = new QTimeLine {500, q};

  QObject::connect(timeLine,
                   &QTimeLine::valueChanged,
                   q,
                   [this](auto value) { slotTimeLineChanged(value); });
  QObject::connect(
      timeLine, &QTimeLine::finished, q, [this] { slotTimeLineFinished(); });

  iconLabel = new QLabel {q};
  iconLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  iconLabel->hide();

  textLabel = new QLabel {q};
  textLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  QObject::connect(
      textLabel, &QLabel::linkActivated, q, &MessageWidget::linkActivated);
  QObject::connect(
      textLabel, &QLabel::linkHovered, q, &MessageWidget::linkHovered);

  auto* close_action {new QAction {q}};
  close_action->setText(QObject::tr("&Close", "@action:button"));
  close_action->setToolTip(QObject::tr("Close message", "@info:tooltip"));
  QStyleOptionFrame opt;
  opt.initFrom(q);
  close_action->setIcon(
      q->style()->standardIcon(QStyle::SP_DialogCloseButton, &opt, q));

  QObject::connect(
      close_action, &QAction::triggered, q, &MessageWidget::animatedHide);

  closeButton = new QToolButton {q};
  closeButton->setAutoRaise(true);
  closeButton->setDefaultAction(close_action);

  q->connect(qApp,
             &QApplication::paletteChanged,
             q,
             [&] { MessageWidgetPrivate::setPalette(); });
}

void MessageWidgetPrivate::createLayout()
{
  delete q->layout();

  qDeleteAll(buttons);
  buttons.clear();

  const auto actions {q->actions()};
  buttons.reserve(actions.size());
  std::for_each(actions.begin(),
                actions.end(),
                [=](auto* action)
                {
                  auto* button {new QToolButton {q}};
                  button->setDefaultAction(action);
                  button->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                  auto* previous {buttons.empty()
                                      ? static_cast<QWidget*>(textLabel)
                                      : buttons.back()};
                  QWidget::setTabOrder(previous, button);
                  buttons.emplace_back(button);
                });

  closeButton->setAutoRaise(buttons.empty());
  if (wordWrap) {
    auto* layout {new QGridLayout {q}};

    layout->addWidget(iconLabel, 0, 0, 1, 1, Qt::AlignHCenter | Qt::AlignTop);
    layout->addWidget(textLabel, 0, 1);

    if (buttons.empty()) {
      layout->addWidget(
          closeButton, 0, 2, 1, 1, Qt::AlignHCenter | Qt::AlignTop);
    } else {
      auto* button_layout {new QHBoxLayout};
      button_layout->addStretch();
      std::for_each(buttons.begin(),
                    buttons.end(),
                    [=](auto* button)
                    {
                      button->show();
                      button_layout->addWidget(button);
                    });
      button_layout->addWidget(closeButton);
      layout->addItem(button_layout, 1, 0, 1, 2);
    }
  } else {
    auto* layout {new QHBoxLayout {q}};
    layout->addWidget(iconLabel, 0, Qt::AlignTop);
    layout->addWidget(textLabel);

    std::for_each(buttons.begin(),
                  buttons.end(),
                  [&layout](auto* button)
                  { layout->addWidget(button, 0, Qt::AlignTop); });
    layout->addWidget(closeButton, 0, Qt::AlignTop);
  }

  q->layout()->setContentsMargins(q->layout()->contentsMargins() + border_size);
  if (q->isVisible()) {
    q->setFixedHeight(q->sizeHint().height());
  }
  q->updateGeometry();
}

void MessageWidgetPrivate::setPalette() const
{
  auto palette {q->palette()};

  const auto bg_base_color {[&]
                            {
                              switch (messageType) {
                                case MessageWidget::MessageType::Positive:
                                  return QColor {39, 174, 96};
                                case MessageWidget::MessageType::Information:
                                  return QColor {61, 174, 233};
                                case MessageWidget::MessageType::Warning:
                                  return QColor {246, 116, 0};
                                case MessageWidget::MessageType::Error:
                                  return QColor {218, 68, 83};
                              }
                            }()};
  palette.setColor(QPalette::Window, bg_base_color);

  const auto parent_text_color {(q->parentWidget() != nullptr
                                     ? q->parentWidget()->palette()
                                     : QApplication::palette())
                                    .color(QPalette::WindowText)};
  palette.setColor(QPalette::WindowText, parent_text_color);

  q->setPalette(palette);

  iconLabel->setPalette(palette);
  textLabel->setPalette(palette);

  q->style()->polish(q);
  q->setIcon(icon);

  q->update();
}

void MessageWidgetPrivate::updateLayout()
{
  createLayout();
}

void MessageWidgetPrivate::slotTimeLineChanged(double value) const
{
  q->setFixedHeight(
      static_cast<int>(std::min(value * 2, 1.0) * bestContentHeight()));
  q->update();
}

void MessageWidgetPrivate::slotTimeLineFinished() const
{
  if (timeLine->direction() == QTimeLine::Forward) {
    q->resize(q->width(), bestContentHeight());
  } else {
    q->hide();
  }

  emit q->hideAnimationFinished();
}

auto MessageWidgetPrivate::bestContentHeight() const -> int
{
  const auto height {q->heightForWidth(q->width())};
  if (height == -1) {
    return q->sizeHint().height();
  }
  return height;
}

// ------------------------------------------------------------------------

MessageWidget::MessageWidget(QWidget* parent)
    : QFrame {parent}
    , d {std::make_unique<MessageWidgetPrivate>()}
{
  d->init(this);
}

MessageWidget::MessageWidget(const QString& text, QWidget* parent)
    : QFrame {parent}
    , d {std::make_unique<MessageWidgetPrivate>()}
{
  d->init(this);
  setText(text);
}

MessageWidget::~MessageWidget() = default;

auto MessageWidget::text() const -> QString
{
  return d->textLabel->text();
}

auto MessageWidget::wordWrap() const -> bool
{
  return d->wordWrap;
}

auto MessageWidget::isCloseButtonVisible() const -> bool
{
  return d->closeButton->isVisible();
}

auto MessageWidget::messageType() const -> MessageWidget::MessageType
{
  return d->messageType;
}

void MessageWidget::addAction(QAction* action)
{
  QFrame::addAction(action);
  d->updateLayout();
}

void MessageWidget::removeAction(QAction* action)
{
  QFrame::removeAction(action);
  d->updateLayout();
}

void MessageWidget::clearActions()
{
  auto our_actions {actions()};
  std::for_each(our_actions.begin(),
                our_actions.end(),
                [=](auto* action) { removeAction(action); });
  d->updateLayout();
}

auto MessageWidget::sizeHint() const -> QSize
{
  ensurePolished();
  return QFrame::sizeHint();
}

auto MessageWidget::minimumSizeHint() const -> QSize
{
  ensurePolished();
  return QFrame::minimumSizeHint();
}

auto MessageWidget::heightForWidth(int width) const -> int
{
  ensurePolished();
  return QFrame::heightForWidth(width);
}

auto MessageWidget::icon() const -> QIcon
{
  return d->icon;
}

auto MessageWidget::isHideAnimationRunning() const -> bool
{
  return (d->timeLine->direction() == QTimeLine::Backward)
      && (d->timeLine->state() == QTimeLine::Running);
}

auto MessageWidget::isShowAnimationRunning() const -> bool
{
  return (d->timeLine->direction() == QTimeLine::Forward)
      && (d->timeLine->state() == QTimeLine::Running);
}

void MessageWidget::paintEvent(QPaintEvent* event)
{
  Q_UNUSED(event)
  QPainter painter {this};
  if (d->timeLine->state() == QTimeLine::Running) {
    painter.setOpacity(d->timeLine->currentValue()
                       * d->timeLine->currentValue());
  }
  constexpr auto radius {4 * 0.6};
  const auto inner_rect {rect().marginsRemoved(QMargins {} + border_size / 2)};
  const auto color {palette().color(QPalette::Window)};
  constexpr auto alpha {0.2};
  const auto parent_window_color {(parentWidget() != nullptr
                                       ? parentWidget()->palette()
                                       : QApplication::palette())
                                      .color(QPalette::Window)};
  const auto new_red {static_cast<int>(
      (color.red() * alpha) + (parent_window_color.red() * (1 - alpha)))};
  const auto new_green {static_cast<int>(
      (color.green() * alpha) + (parent_window_color.green() * (1 - alpha)))};
  const auto new_blue {static_cast<int>(
      (color.blue() * alpha) + (parent_window_color.blue() * (1 - alpha)))};
  painter.setPen(QPen {color, border_size});
  painter.setBrush(QColor {new_red, new_green, new_blue});
  painter.setRenderHint(QPainter::Antialiasing);
  painter.drawRoundedRect(inner_rect, radius, radius);
}

void MessageWidget::resizeEvent(QResizeEvent* event)
{
  QFrame::resizeEvent(event);
  if (d->timeLine->state() == QTimeLine::NotRunning
      && d->ignoreShowAndResizeEventDoingAnimatedShow)
  {
    setFixedHeight(d->bestContentHeight());
  }
}

auto MessageWidget::event(QEvent* event) -> bool
{
  if (event->type() == QEvent::Polish && layout() == nullptr) {
    d->createLayout();
  } else if (event->type() == QEvent::Show
             && !d->ignoreShowAndResizeEventDoingAnimatedShow)
  {
    setFixedHeight(d->bestContentHeight());
  } else if (event->type() == QEvent::ParentChange) {
    d->setPalette();
  }
  return QFrame::event(event);
}

void MessageWidget::setText(const QString& text)
{
  d->textLabel->setText(text);
  updateGeometry();
}

void MessageWidget::setWordWrap(bool word_wrap)
{
  d->wordWrap = word_wrap;
  d->textLabel->setWordWrap(word_wrap);
  auto policy {sizePolicy()};
  policy.setHeightForWidth(word_wrap);
  setSizePolicy(policy);
  d->updateLayout();
  if (word_wrap) {
    setMinimumHeight(0);
  }
}

void MessageWidget::setCloseButtonVisible(bool visible)
{
  d->closeButton->setVisible(visible);
  updateGeometry();
}

void MessageWidget::setMessageType(MessageType type)
{
  d->messageType = type;
  d->setPalette();
}

void MessageWidget::animatedShow()
{
  if (isHideAnimationRunning()) {
    d->timeLine->stop();
    emit hideAnimationFinished();
  }

  if ((style()->styleHint(QStyle::SH_Widget_Animate, nullptr, this) == 0)
      || (parentWidget() != nullptr && !parentWidget()->isVisible()))
  {
    show();
    emit showAnimationFinished();
    return;
  }

  if (isVisible() && (d->timeLine->state() == QTimeLine::NotRunning)
      && (height() == d->bestContentHeight()))
  {
    emit showAnimationFinished();
    return;
  }

  d->ignoreShowAndResizeEventDoingAnimatedShow = true;
  show();
  d->ignoreShowAndResizeEventDoingAnimatedShow = false;
  setFixedHeight(0);

  d->timeLine->setDirection(QTimeLine::Forward);
  if (d->timeLine->state() == QTimeLine::NotRunning) {
    d->timeLine->start();
  }
}

void MessageWidget::animatedHide()
{
  if (isShowAnimationRunning()) {
    d->timeLine->stop();
    emit showAnimationFinished();
  }

  if (style()->styleHint(QStyle::SH_Widget_Animate, nullptr, this) == 0) {
    hide();
    emit hideAnimationFinished();
    return;
  }

  if (!isVisible()) {
    hide();
    emit hideAnimationFinished();
    return;
  }

  d->timeLine->setDirection(QTimeLine::Backward);
  if (d->timeLine->state() == QTimeLine::NotRunning) {
    d->timeLine->start();
  }
}

void MessageWidget::setIcon(const QIcon& icon)
{
  d->icon = icon;
  if (d->icon.isNull()) {
    d->iconLabel->hide();
  } else {
    const int size {style()->pixelMetric(QStyle::PM_ToolBarIconSize)};
    d->iconLabel->setPixmap(d->icon.pixmap(size));
    d->iconLabel->show();
  }
}

}  // namespace addons

#include "moc_message_widget.cpp"
