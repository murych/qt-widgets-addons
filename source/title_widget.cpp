/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 2007 Urs Wolfer <uwolfer@kde.org>
    SPDX-FileCopyrightText: 2007 Michaël Larouche <larouche@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include <QApplication>
#include <QDebug>
#include <QFrame>
#include <QIcon>
#include <QLabel>
#include <QLayout>
#include <QMouseEvent>
#include <QStyle>
#include <QTextDocument>
#include <QTimer>

#include "title_widget.h"

namespace addons
{

struct TitleWidgetPrivate
{
  int level {1};
  TitleWidget* const q;

  QGridLayout* headerLayout {nullptr};
  QLabel* imageLabel {nullptr};
  QLabel* textLabel {nullptr};
  QLabel* commentLabel {nullptr};
  QIcon icon;
  QSize iconSize;
  TitleWidget::ImageAlignment iconAlignment {
      TitleWidget::ImageAlignment::ImageLeft};
  TitleWidget::MessageType messageType {TitleWidget::MessageType::InfoMessage};
  int autoHideTimeout {0};

  explicit TitleWidgetPrivate(TitleWidget* parent)
      : q {parent}
  {
  }

  [[nodiscard]] auto textStyleSheet() const
  {
    const auto factor {[&]
                       {
                         switch (level) {
                           case 1:
                             return 1.35;
                           case 2:
                             return 1.20;
                           case 3:
                             return 1.15;
                           case 4:
                             return 1.10;
                           default:
                             return 1.0;
                         }
                       }()};
    const auto font_size {QApplication::font().pointSize() * factor};
    return QStringLiteral("QLabel { font-size: %1pt; color: %2 }")
        .arg(QString::number(font_size),
             q->palette().color(QPalette::WindowText).name());
  }

  [[nodiscard]] auto commentStyleSheet() const
  {
    switch (messageType) {
      // FIXME: we need the usability color styles to implement different
      //       yet palette appropriate colours for the different use cases!
      //       also .. should we include an icon here,
      //       perhaps using the imageLabel?
      case TitleWidget::MessageType::InfoMessage:
      case TitleWidget::MessageType::WarningMessage:
      case TitleWidget::MessageType::ErrorMessage:
        //        styleSheet =
        return QStringLiteral(
                   "QLabel { color: palette(%1); background: palette(%2); }")
            .arg(q->palette().color(QPalette::HighlightedText).name(),
                 q->palette().color(QPalette::Highlight).name());
        break;
      case TitleWidget::MessageType::PlainMessage:
      default:
        break;
    }
    return QStringLiteral("");
  }

  void updateIconAlignment(TitleWidget::ImageAlignment new_icon_alignment)
  {
    if (iconAlignment == new_icon_alignment) {
      return;
    }

    iconAlignment = new_icon_alignment;

    headerLayout->removeWidget(textLabel);
    headerLayout->removeWidget(commentLabel);
    headerLayout->removeWidget(imageLabel);

    if (iconAlignment == TitleWidget::ImageAlignment::ImageLeft) {
      // swap the text and image labels around
      headerLayout->addWidget(imageLabel, 0, 0, 2, 1);
      headerLayout->addWidget(textLabel, 0, 1);
      headerLayout->addWidget(commentLabel, 1, 1);
      headerLayout->setColumnStretch(0, 0);
      headerLayout->setColumnStretch(1, 1);
    } else {
      headerLayout->addWidget(textLabel, 0, 0);
      headerLayout->addWidget(commentLabel, 1, 0);
      headerLayout->addWidget(imageLabel, 0, 1, 2, 1);
      headerLayout->setColumnStretch(1, 0);
      headerLayout->setColumnStretch(0, 1);
    }
  }

  void updatePixmap() const
  {
    const auto pixmap {icon.pixmap(q->iconSize())};
    imageLabel->setPixmap(pixmap);
  }

  [[nodiscard]] auto iconTypeToIconName(TitleWidget::MessageType type) const
  {
    switch (type) {
      case TitleWidget::MessageType::InfoMessage:
        return QStringLiteral("dialog-information");
      case TitleWidget::MessageType::ErrorMessage:
        return QStringLiteral("dialog-error");
      case TitleWidget::MessageType::WarningMessage:
        return QStringLiteral("dialog-warning");
      case TitleWidget::MessageType::PlainMessage:
        break;
    }

    return QStringLiteral("");
  }

  void timeoutFinished() const { q->setVisible(false); }
};

TitleWidget::TitleWidget(QWidget* parent)
    : QWidget {parent}
    , d {std::make_unique<TitleWidgetPrivate>(this)}
{
  auto* title_frame {new QFrame {this}};
  title_frame->setAutoFillBackground(true);
  title_frame->setFrameShape(QFrame::StyledPanel);
  title_frame->setFrameShadow(QFrame::Plain);
  title_frame->setBackgroundRole(QPalette::Window);
  title_frame->setContentsMargins(0, 0, 0, 0);

  // default image / text part start
  d->headerLayout = new QGridLayout {title_frame};
  d->headerLayout->setContentsMargins(0, 0, 0, 0);

  d->textLabel = new QLabel {title_frame};
  d->textLabel->setVisible(false);

  d->imageLabel = new QLabel {title_frame};
  d->imageLabel->setVisible(false);

  d->commentLabel = new QLabel {title_frame};
  d->commentLabel->setVisible(false);
  d->commentLabel->setWordWrap(true);

  // make sure d->iconAlignment is left, to trigger initial layout default image
  // / text part end
  d->updateIconAlignment(ImageAlignment::ImageRight);

  auto* mainLayout {new QVBoxLayout {this}};
  mainLayout->addWidget(title_frame);
  mainLayout->setContentsMargins(0, 0, 0, 0);
}

TitleWidget::~TitleWidget() = default;

void TitleWidget::setWidget(QWidget* widget)
{
  d->headerLayout->addWidget(widget, 2, 0, 1, 2);
}

QString TitleWidget::text() const
{
  return d->textLabel->text();
}

QString TitleWidget::comment() const
{
  return d->commentLabel->text();
}

QIcon TitleWidget::icon() const
{
  return d->icon;
}

QSize TitleWidget::iconSize() const
{
  if (d->iconSize.isValid()) {
    return d->iconSize;
  }

  const auto icon_size_extent {
      style()->pixelMetric(QStyle::PM_MessageBoxIconSize)};
  return {icon_size_extent, icon_size_extent};
}

void TitleWidget::setBuddy(QWidget* buddy)
{
  d->textLabel->setBuddy(buddy);
}

int TitleWidget::autoHideTimeout() const
{
  return d->autoHideTimeout;
}

int TitleWidget::level()
{
  return d->level;
}

void TitleWidget::changeEvent(QEvent* e)
{
  switch (e->type()) {
    case QEvent::PaletteChange:
    case QEvent::FontChange:
    case QEvent::ApplicationFontChange:
      d->textLabel->setStyleSheet(d->textStyleSheet());
      d->commentLabel->setStyleSheet(d->commentStyleSheet());
      d->updatePixmap();
      break;
    case QEvent::StyleChange:
      if (!d->iconSize.isValid()) {
        // relies on style's PM_MessageBoxIconSize
        d->updatePixmap();
      }
      break;
    default:
      break;
  }
}

void TitleWidget::showEvent(QShowEvent* event)
{
  Q_UNUSED(event)
  if (d->autoHideTimeout > 0) {
    QTimer::singleShot(d->autoHideTimeout, this, [&] { d->timeoutFinished(); });
  }
}

bool TitleWidget::eventFilter(QObject* object, QEvent* event)
{
  // hide message label on click
  if (d->autoHideTimeout > 0 && event->type() == QEvent::MouseButtonPress) {
    auto* mouse_event {dynamic_cast<QMouseEvent*>(event)};
    if (mouse_event != nullptr && mouse_event->button() == Qt::LeftButton) {
      setVisible(false);
      return true;
    }
  }
  return QWidget::eventFilter(object, event);
}

void TitleWidget::setText(const QString& text, Qt::Alignment alignment)
{
  d->textLabel->setVisible(!text.isNull());
  if (!Qt::mightBeRichText(text)) {
    d->textLabel->setStyleSheet(d->textStyleSheet());
  }

  d->textLabel->setText(text);
  d->textLabel->setAlignment(alignment);
  show();
}

void TitleWidget::setText(const QString& text, MessageType type)
{
  setIcon(type);
  setText(text);
}

void TitleWidget::setComment(const QString& comment, MessageType type)
{
  d->commentLabel->setVisible(!comment.isNull());

  // TODO: should we override the current icon with the corresponding
  // MessageType icon?
  d->messageType = type;
  d->commentLabel->setStyleSheet(d->commentStyleSheet());
  d->commentLabel->setText(comment);
  show();
}

void TitleWidget::setIcon(const QIcon& icon, ImageAlignment alignment)
{
  d->icon = icon;
  d->imageLabel->setVisible(!icon.isNull());

  d->updateIconAlignment(alignment);
  d->updatePixmap();
}

void TitleWidget::setIcon(MessageType type, ImageAlignment alignment)
{
  setIcon(QIcon::fromTheme(d->iconTypeToIconName(type)), alignment);
}

void TitleWidget::setIconSize(const QSize& icon_size)
{
  if (d->iconSize == icon_size) {
    return;
  }

  d->iconSize = icon_size;
  const auto old_effective_icon_size {iconSize()};
  if (old_effective_icon_size != this->iconSize()) {
    d->updatePixmap();
  }
}

void TitleWidget::setAutoHideTimeout(int msecs)
{
  d->autoHideTimeout = msecs;

  if (msecs > 0) {
    installEventFilter(this);
  } else {
    removeEventFilter(this);
  }
}

void TitleWidget::setLevel(int level)
{
  if (d->level == level) {
    return;
  }

  d->level = level;
  d->textLabel->setStyleSheet(d->textStyleSheet());
}

}  // namespace addons

#include "moc_title_widget.cpp"
