#ifndef UTILS_H
#define UTILS_H

#include <utility>

#include <widgets-addons/widgets-addons_export.hpp>

namespace addons
{

/*!
 * \brief priv_slot
 * \param dptr
 * \param slot
 *
 * https://www.dvratil.cz/2019/11/q-private-slot-with-new-connect-syntax/
 */
template<typename DPtr, typename Slot>
auto WIDGETS_ADDONS_NO_EXPORT priv_slot(DPtr&& dptr, Slot&& slot)
{
  return [&dptr, &slot](auto&&... args)
  { (dptr->*slot)(std::forward<decltype(args)>(args)...); };
}

}  // namespace addons

#endif  // UTILS_H
