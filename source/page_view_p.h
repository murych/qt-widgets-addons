/*
    This file is part of the KDE Libraries
    SPDX-FileCopyrightText: 2006 Tobias Koenig <tokoe@kde.org>
    SPDX-FileCopyrightText: 2007 Rafael Fernández López <ereslibre@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef WIDGETS_PAGEVIEWPRIVATE_H
#define WIDGETS_PAGEVIEWPRIVATE_H

#include <QAbstractItemDelegate>
#include <QAbstractItemView>
#include <QAbstractProxyModel>
#include <QGridLayout>
#include <QListView>
#include <QModelIndex>
#include <QPointer>
#include <QStackedWidget>
#include <QTreeView>

#include "page_view.h"
#include "title_widget.h"

namespace addons
{
class PageStackedWidget : public QStackedWidget
{
  Q_OBJECT
public:
  explicit PageStackedWidget(QWidget* parent = nullptr)
      : QStackedWidget(parent)
  {
  }

  void setMinimumSize(const QSize& size) { mMinimumSize = size; }

  QSize minimumSizeHint() const override
  {
    return mMinimumSize.expandedTo(QStackedWidget::minimumSizeHint());
  }

private:
  QSize mMinimumSize;
};

class PageViewPrivate
{
  Q_DECLARE_PUBLIC(PageView)

public:
  virtual ~PageViewPrivate() = default;

protected:
  PageViewPrivate(PageView*);

  PageView* q_ptr;

  // data
  QAbstractItemModel* model;
  PageView::FaceType faceType;

  // gui
  QGridLayout* layout;
  PageStackedWidget* stack;
  TitleWidget* titleWidget;
  QWidget* defaultWidget;

  QAbstractItemView* view;

  QPointer<QWidget> pageHeader;
  QPointer<QWidget> pageFooter;

  void updateTitleWidget(const QModelIndex& index);

  void updateSelection();
  void cleanupPages();
  QList<QWidget*> collectPages(const QModelIndex& parent = QModelIndex());
  PageView::FaceType detectAutoFace() const;
  PageView::FaceType effectiveFaceType() const;

  // private slots
  void rebuildGui();
  void modelChanged();
  void dataChanged(const QModelIndex&, const QModelIndex&);
  void pageSelected(const QItemSelection&, const QItemSelection&);

private:
  void init();

  QMetaObject::Connection m_dataChangedConnection;
  QMetaObject::Connection m_layoutChangedConnection;
  QMetaObject::Connection m_selectionChangedConnection;
};

namespace KDEPrivate
{
class PageListViewDelegate;
class PageListViewProxy;

class PagePlainView : public QAbstractItemView
{
  Q_OBJECT
public:
  explicit PagePlainView(QWidget* parent = nullptr);

  QModelIndex indexAt(const QPoint& point) const override;
  void scrollTo(const QModelIndex& index,
                ScrollHint hint = EnsureVisible) override;
  QRect visualRect(const QModelIndex& index) const override;

protected:
  QModelIndex moveCursor(QAbstractItemView::CursorAction,
                         Qt::KeyboardModifiers) override;
  int horizontalOffset() const override;
  int verticalOffset() const override;
  bool isIndexHidden(const QModelIndex&) const override;
  void setSelection(const QRect&,
                    QFlags<QItemSelectionModel::SelectionFlag>) override;
  QRegion visualRegionForSelection(const QItemSelection&) const override;
};

class PageListView : public QListView
{
  Q_OBJECT

public:
  explicit PageListView(QWidget* parent = nullptr);
  ~PageListView() override;

  void setModel(QAbstractItemModel* model) override;

protected:
  void changeEvent(QEvent* event) override;

private Q_SLOTS:
  void updateWidth();
};

class PageTreeView : public QTreeView
{
  Q_OBJECT

public:
  explicit PageTreeView(QWidget* parent = nullptr);

  void setModel(QAbstractItemModel* model) override;

private Q_SLOTS:
  void updateWidth();

private:
  void expandItems(const QModelIndex& index = QModelIndex());
};

class PageTabbedView : public QAbstractItemView
{
  Q_OBJECT

public:
  explicit PageTabbedView(QWidget* parent = nullptr);
  ~PageTabbedView() override;

  void setModel(QAbstractItemModel* model) override;

  QModelIndex indexAt(const QPoint& point) const override;
  void scrollTo(const QModelIndex& index,
                ScrollHint hint = EnsureVisible) override;
  QRect visualRect(const QModelIndex& index) const override;

  QSize minimumSizeHint() const override;

protected:
  QModelIndex moveCursor(QAbstractItemView::CursorAction,
                         Qt::KeyboardModifiers) override;
  int horizontalOffset() const override;
  int verticalOffset() const override;
  bool isIndexHidden(const QModelIndex&) const override;
  void setSelection(const QRect&,
                    QFlags<QItemSelectionModel::SelectionFlag>) override;
  QRegion visualRegionForSelection(const QItemSelection&) const override;

private Q_SLOTS:
  void currentPageChanged(int);
  void layoutChanged();
  void dataChanged(const QModelIndex&,
                   const QModelIndex&,
                   const QVector<int>& roles) override;

private:
  QTabWidget* mTabWidget;
};

class PageListViewDelegate : public QAbstractItemDelegate
{
  Q_OBJECT

public:
  explicit PageListViewDelegate(QObject* parent = nullptr);

  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;
  QSize sizeHint(const QStyleOptionViewItem& option,
                 const QModelIndex& index) const override;

private:
  void drawFocus(QPainter*, const QStyleOptionViewItem&, const QRect&) const;
};

/**
 * We need this proxy model to map the leaves of a tree-like model
 * to a one-level list model.
 */
class PageListViewProxy : public QAbstractProxyModel
{
  Q_OBJECT

public:
  explicit PageListViewProxy(QObject* parent = nullptr);
  ~PageListViewProxy() override;

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QModelIndex index(int row,
                    int column,
                    const QModelIndex& parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex&) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  QModelIndex mapFromSource(const QModelIndex& index) const override;
  QModelIndex mapToSource(const QModelIndex& index) const override;

public Q_SLOTS:
  void rebuildMap();

private:
  void addMapEntry(const QModelIndex&);

  QList<QModelIndex> mList;
};

class SelectionModel : public QItemSelectionModel
{
  Q_OBJECT

public:
  SelectionModel(QAbstractItemModel* model, QObject* parent);

public Q_SLOTS:
  void clear() override;
  void select(const QModelIndex& index,
              QItemSelectionModel::SelectionFlags command) override;
  void select(const QItemSelection& selection,
              QItemSelectionModel::SelectionFlags command) override;
};

}  // namespace KDEPrivate

}  // namespace addons

#endif  // WIDGETS_PAGEVIEWPRIVATE_H
