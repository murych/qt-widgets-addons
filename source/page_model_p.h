/*
    This file is part of the KDE project
    SPDX-FileCopyrightText: 2007 Matthias Kretz <kretz@kde.org>

 SPDX-License-Identifier: LGPL-2.0-only
*/

#ifndef MODELS_PAGEMODELPRIVATE_H
#define MODELS_PAGEMODELPRIVATE_H

#include "page_model.h"

namespace addons
{

class PageModelPrivate
{
  Q_DECLARE_PUBLIC(PageModel)
public:
  virtual ~PageModelPrivate();

protected:
  PageModel* q_ptr;
};

}  // namespace addons

#endif  // PageModel_P_H
