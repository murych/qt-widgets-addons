/*
    This file is part of the KDE Libraries
    SPDX-FileCopyrightText: 2006 Tobias Koenig <tokoe@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "page_model.h"

#include "page_model_p.h"

namespace addons
{

PageModelPrivate::~PageModelPrivate() {}

PageModel::PageModel(QObject* parent)
    : QAbstractItemModel(parent)
    , d_ptr(nullptr)
{
}

PageModel::PageModel(PageModelPrivate& dd, QObject* parent)
    : QAbstractItemModel(parent)
    , d_ptr(&dd)
{
  d_ptr->q_ptr = this;
}

PageModel::~PageModel() = default;

}  // namespace addons
