#include <QMouseEvent>
#include <QPainter>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <cmath>
#include <utility>

#include "joypad.h"

namespace addons
{

template<typename T>
T constrain(T Value, T Min, T Max)
{
  return (Value < Min) ? Min : (Value > Max) ? Max : Value;
}

JoyPad::JoyPad(QWidget* parent)
    : QWidget {parent}
    , m_returnAnimation {std::make_unique<QParallelAnimationGroup>(this)}
    , m_xAnimation {std::make_unique<QPropertyAnimation>(this, "x")}
    , m_yAnimation {std::make_unique<QPropertyAnimation>(this, "y")}
{
  m_xAnimation->setEndValue(0.F);
  m_xAnimation->setDuration(400);
  m_xAnimation->setEasingCurve(QEasingCurve::OutSine);

  m_yAnimation->setEndValue(0.F);
  m_yAnimation->setDuration(400);
  m_yAnimation->setEasingCurve(QEasingCurve::OutSine);

  m_returnAnimation->addAnimation(m_xAnimation.get());
  m_returnAnimation->addAnimation(m_yAnimation.get());
}

JoyPad::~JoyPad() = default;

/**
 * @brief JoyPad::setX
 * @param value of x axis from -1 to 1
 */
void JoyPad::setX(float value)
{
  m_x = constrain(value, -1.F, 1.F);

  const auto radius {(m_bounds.width() - m_knopBounds.width()) / 2};
  m_knopBounds.moveCenter(QPointF {m_bounds.center().x() + m_x * radius,
                                   m_knopBounds.center().y()});

  update();
  emit xChanged(m_x);
}

/**
 * @brief JoyPad::setY
 * @param value of y axis from -1 to 1
 */
void JoyPad::setY(float value)
{
  m_y = constrain(value, -1.F, 1.F);

  const auto radius = (m_bounds.width() - m_knopBounds.width()) / 2;
  m_knopBounds.moveCenter(QPointF {m_knopBounds.center().x(),
                                   m_bounds.center().y() + m_y * radius});

  update();
  emit yChanged(m_y);
}

void JoyPad::removeXAnimation()
{
  // return if the animation is already removed
  if (m_xAnimation->parent() != m_returnAnimation.get()) {
    return;
  }

  m_returnAnimation->removeAnimation(m_xAnimation.get());

  // take ownership of the animation (parent is 0 after removeAnimation())
  m_xAnimation->setParent(this);
}

void JoyPad::addXAnimation()
{
  // abort if the animation is already added
  if (m_xAnimation->parent() == m_returnAnimation.get()) {
    return;
  }

  m_returnAnimation->addAnimation(m_xAnimation.get());
}

void JoyPad::removeYAnimation()
{
  if (m_yAnimation->parent() != m_returnAnimation.get()) {
    return;
  }

  m_returnAnimation->removeAnimation(m_yAnimation.get());
  m_yAnimation->setParent(this);
}

void JoyPad::addYAnimation()
{
  if (m_yAnimation->parent() == m_returnAnimation.get()) {
    return;
  }

  m_returnAnimation->addAnimation(m_yAnimation.get());
}

void JoyPad::setAlignment(Qt::Alignment f)
{
  m_alignment = f;
}

/**
 * @brief JoyPad::resizeEvent
 * @param event
 *
 * calculates a square bounding rect for the background and the knob
 */
void JoyPad::resizeEvent(QResizeEvent* event)
{
  Q_UNUSED(event)

  const auto size {std::min(rect().width(), rect().height())};

  QPointF topleft;

  if (m_alignment.testFlag(Qt::AlignTop)) {
    topleft.setY(0);
  } else if (m_alignment.testFlag(Qt::AlignVCenter)) {
    topleft.setY(((height() - size) / 2));
  } else if (m_alignment.testFlag(Qt::AlignBottom)) {
    topleft.setY(height() - size);
  }

  if (m_alignment.testFlag(Qt::AlignLeft)) {
    topleft.setX(0);
  } else if (m_alignment.testFlag(Qt::AlignHCenter)) {
    topleft.setX((width() - size) / 2);
  } else if (m_alignment.testFlag(Qt::AlignRight)) {
    topleft.setX(width() - size);
  }

  m_bounds = QRectF {topleft, QSize {size, size}};

  m_knopBounds.setWidth(size * 0.3);
  m_knopBounds.setHeight(size * 0.3);

  // adjust knob position
  const auto radius = (m_bounds.width() - m_knopBounds.width()) / 2;
  m_knopBounds.moveCenter(QPointF(m_bounds.center().x() + m_x * radius,
                                  m_bounds.center().y() - m_y * radius));
}

/**
 * @brief JoyPad::paintEvent
 * @param event
 */
void JoyPad::paintEvent(QPaintEvent* event)
{
  Q_UNUSED(event)

  QPainter painter {this};
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::HighQualityAntialiasing);

  // draw background
  QRadialGradient gradient {
      m_bounds.center(), m_bounds.width() / 2, m_bounds.center()};
  gradient.setFocalRadius(m_bounds.width() * 0.3);
  gradient.setCenterRadius(m_bounds.width() * 0.7);
  gradient.setColorAt(0, Qt::white);
  gradient.setColorAt(1, Qt::lightGray);

  painter.setPen(QPen {QBrush {Qt::gray}, m_bounds.width() * 0.005});
  painter.setBrush(QBrush {gradient});
  painter.drawEllipse(m_bounds);

  // draw crosshair
  painter.setPen(QPen {QBrush {Qt::gray}, m_bounds.width() * 0.005});
  painter.drawLine(QPointF {m_bounds.left(), m_bounds.center().y()},
                   QPointF {m_bounds.center().x() - m_bounds.width() * 0.35,
                            m_bounds.center().y()});
  painter.drawLine(QPointF {m_bounds.center().x() + m_bounds.width() * 0.35,
                            m_bounds.center().y()},
                   QPointF {m_bounds.right(), m_bounds.center().y()});
  painter.drawLine(QPointF {m_bounds.center().x(), m_bounds.top()},
                   QPointF {m_bounds.center().x(),
                            m_bounds.center().y() - m_bounds.width() * 0.35});
  painter.drawLine(QPointF {m_bounds.center().x(),
                            m_bounds.center().y() + m_bounds.width() * 0.35},
                   QPointF {m_bounds.center().x(), m_bounds.bottom()});

  // draw knob
  if (!this->isEnabled()) {
    return;
  }

  gradient = QRadialGradient {
      m_knopBounds.center(), m_knopBounds.width() / 2, m_knopBounds.center()};
  gradient.setColorAt(0, Qt::red);
  gradient.setColorAt(1, Qt::darkRed);
  gradient.setFocalRadius(m_knopBounds.width() * 0.2);
  gradient.setCenterRadius(m_knopBounds.width() * 0.5);

  painter.setPen(QPen {QBrush {Qt::darkRed}, m_bounds.width() * 0.005});
  painter.setBrush(QBrush {gradient});
  painter.drawEllipse(m_knopBounds);
}

/**
 * @brief JoyPad::mousePressEvent
 * @param event
 */
void JoyPad::mousePressEvent(QMouseEvent* event)
{
  if (m_knopBounds.contains(event->pos())) {
    m_returnAnimation->stop();
    m_lastPos = event->pos();
    knopPressed = true;
  }
}

/**
 * @brief JoyPad::mouseReleaseEvent
 * @param event
 */
void JoyPad::mouseReleaseEvent(QMouseEvent* event)
{
  Q_UNUSED(event)

  knopPressed = false;
  m_returnAnimation->start();
}

/**
 * @brief JoyPad::mouseMoveEvent
 * @param event
 */
void JoyPad::mouseMoveEvent(QMouseEvent* event)
{
  if (!knopPressed) {
    return;
  }

  // moved distance
  auto dPos {QPointF {event->pos() - m_lastPos}};
  // change the distance sligthly to guarantee overlaping knop and pointer
  dPos += 0.05 * (event->pos() - m_knopBounds.center());

  auto fromCenterToKnop {m_knopBounds.center() + dPos - m_bounds.center()};
  auto const radius {(m_bounds.width() - m_knopBounds.width()) / 2};

  fromCenterToKnop.setX(constrain(fromCenterToKnop.x(), -radius, radius));
  fromCenterToKnop.setY(constrain(fromCenterToKnop.y(), -radius, radius));

  m_knopBounds.moveCenter(fromCenterToKnop + m_bounds.center());
  m_lastPos = event->pos();

  update();

  if (qFuzzyCompare(radius, 0)) {
    return;
  }

  const float x {static_cast<float>(
      (m_knopBounds.center().x() - m_bounds.center().x()) / radius)};
  const float y {static_cast<float>(
      (m_knopBounds.center().y() - m_bounds.center().y()) / radius)};

  if (!qFuzzyCompare(m_x, x)) {
    m_x = x;
    emit xChanged(m_x);
  }

  if (!qFuzzyCompare(m_y, y)) {
    m_y = y;
    emit yChanged(m_y);
  }
}

}  // namespace addons
