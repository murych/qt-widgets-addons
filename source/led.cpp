/*
    This file is part of the KDE libraries
    SPDX-FileCopyrightText: 1998 Jörg Habenicht <j.habenicht@europemail.com>
    SPDX-FileCopyrightText: 2010 Christoph Feck <cfeck@kde.org>

 SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include <QImage>
#include <QPainter>
#include <QStyle>
#include <QStyleOption>

#include "led.h"

namespace addons
{

class LedPrivate
{
public:
  int darkFactor {300};
  QColor color;
  Led::State state {Led::On};
  Led::Look look {Led::Raised};
  Led::Shape shape {Led::Circular};

  QPixmap cachedPixmap[2];
};

Led::Led(QWidget* parent)
    : QWidget {parent}
    , d {std::make_unique<LedPrivate>()}
{
  setColor(Qt::green);
  updateAccessibleName();
}

Led::Led(const QColor& color, QWidget* parent)
    : QWidget {parent}
    , d {std::make_unique<LedPrivate>()}
{
  setColor(color);
  updateAccessibleName();
}

Led::Led(
    const QColor& color, State state, Look look, Shape shape, QWidget* parent)
    : QWidget {parent}
    , d {std::make_unique<LedPrivate>()}
{
  d->state = (state == Off ? Off : On);
  d->look = look;
  d->shape = shape;

  setColor(color);
  updateAccessibleName();
}

Led::~Led() = default;

QColor Led::color() const
{
  return d->color;
}

Led::State Led::state() const
{
  return d->state;
}

Led::Look Led::look() const
{
  return d->look;
}

Led::Shape Led::shape() const
{
  return d->shape;
}

int Led::darkFactor() const
{
  return d->darkFactor;
}

void Led::setColor(const QColor& color)
{
  if (d->color == color) {
    return;
  }

  d->color = color;
  updateCachedPixmap();
}

void Led::setState(State state)
{
  if (d->state == state) {
    return;
  }

  d->state = (state == Off ? Off : On);
  updateCachedPixmap();
  updateAccessibleName();
}

void Led::setLook(Look look) {}

void Led::setShape(Shape shape)
{
  if (d->shape == shape) {
    return;
  }

  d->shape = shape;
  updateCachedPixmap();
}

void Led::setDarkFactor(int dark_factor)
{
  if (d->darkFactor == dark_factor) {
    return;
  }

  d->darkFactor = dark_factor;
  updateCachedPixmap();
}

QSize Led::sizeHint() const
{
  QStyleOption option;
  option.initFrom(this);

  const auto icon_size {
      style()->pixelMetric(QStyle::PM_SmallIconSize, &option, this)};
  return {icon_size, icon_size};
}

QSize Led::minimumSizeHint() const
{
  return {16, 16};
}

void Led::paintEvent(QPaintEvent*)
{
  if (!d->cachedPixmap[d->state].isNull()) {
    QPainter painter {this};
    painter.drawPixmap(1, 1, d->cachedPixmap[d->state]);
    return;
  }

  QSize size {width() - 2, height() - 2};
  if (d->shape == Circular) {
    const auto dim {std::min(width(), height()) - 2};
    size = QSize {dim, dim};
  }
  QPointF center {size.width() / 2.0, size.height() / 2.0};
  const auto smallest_size {std::min(size.width(), size.height())};

  QPainter painter;
  QImage image {size, QImage::Format_ARGB32_Premultiplied};
  image.fill(0);

  QRadialGradient fill_gradient {
      center, smallest_size / 2.0, QPointF {center.x(), size.height() / 3.0}};
  QColor fill_color {d->state != Off ? d->color
                                     : d->color.darker(d->darkFactor)};

  fill_gradient.setColorAt(0.0, fill_color.lighter(250));
  fill_gradient.setColorAt(0.5, fill_color.lighter(130));
  fill_gradient.setColorAt(1.0, fill_color);

  QConicalGradient border_gradiend {center, d->look == Sunken ? 90.0 : -90.0};
  QColor border_color {palette().color(QPalette::Dark)};
  if (d->state == On) {
    QColor glow_overlay {fill_color};
    glow_overlay.setAlpha(80);

    QImage img {1, 1, QImage::Format_ARGB32_Premultiplied};
    QPainter p {&img};
    QColor start {border_color};

    start.setAlpha(255);
    p.fillRect(0, 0, 1, 1, start);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.fillRect(0, 0, 1, 1, glow_overlay);
    p.end();

    border_color = img.pixel(0, 0);
  }

  border_gradiend.setColorAt(0.2, border_color);
  border_gradiend.setColorAt(0.5, palette().color(QPalette::Light));
  palette().color(QPalette::Light);
  border_gradiend.setColorAt(0.8, border_color);

  painter.begin(&image);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setBrush(d->look == Flat ? QBrush {fill_color}
                                   : QBrush {fill_gradient});

  QBrush pen_brush {d->look == Flat ? QBrush {border_color}
                                    : QBrush {border_gradiend}};
  auto pen_width {smallest_size / 8.0};
  painter.setPen(QPen {pen_brush, pen_width});

  QRectF r {pen_width / 2.0,
            pen_width / 2.0,
            size.width() - pen_width,
            size.height() - pen_width};
  if (d->shape == Rectangular) {
    painter.drawRect(r);
  } else {
    painter.drawEllipse(r);
  }
  painter.end();

  d->cachedPixmap[d->state] = QPixmap::fromImage(image);
  painter.begin(this);
  painter.drawPixmap(1, 1, d->cachedPixmap[d->state]);
  painter.end();
}

void Led::resizeEvent(QResizeEvent*)
{
  updateCachedPixmap();
}

void Led::updateCachedPixmap()
{
  d->cachedPixmap[Off] = QPixmap {};
  d->cachedPixmap[On] = QPixmap {};
  update();
}

void Led::updateAccessibleName()
{
#ifndef QT_NO_ACCESSIBILITY
  auto onName {tr("LED on", "Accessible name of a led whose state is on")};
  auto offName {tr("LED off", "Accessible name of a led whose state is off")};

  auto lastName {accessibleName()};

  if (lastName.isEmpty() || lastName == onName || lastName == offName) {
    setAccessibleName(d->state == On ? onName : offName);
  }
#endif
}

void Led::toggle()
{
  d->state = (d->state == On ? Off : On);
  updateCachedPixmap();
  updateCachedPixmap();
}

void Led::on()
{
  setState(On);
}

void Led::off()
{
  setState(Off);
}

}  // namespace addons

#include "moc_led.cpp"
