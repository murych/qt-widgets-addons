if(PROJECT_IS_TOP_LEVEL)
  set(CMAKE_INSTALL_INCLUDEDIR include/widgets-addons CACHE PATH "")
endif()

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# find_package(<package>) call for consumers to find this project
set(package widgets-addons)

install(
    DIRECTORY
    source/
    "${PROJECT_BINARY_DIR}/export/"
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
    COMPONENT widgets-addons_Development
)

install(
    TARGETS widgets-addons
    EXPORT widgets-addonsTargets
    RUNTIME #
    COMPONENT widgets-addons_Runtime
    LIBRARY #
    COMPONENT widgets-addons_Runtime
    NAMELINK_COMPONENT widgets-addons_Development
    ARCHIVE #
    COMPONENT widgets-addons_Development
    INCLUDES #
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
)

write_basic_package_version_file(
    "${package}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

# Allow package maintainers to freely override the path for the configs
set(
    widgets-addons_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${package}"
    CACHE PATH "CMake package config location relative to the install prefix"
)
mark_as_advanced(widgets-addons_INSTALL_CMAKEDIR)

install(
    FILES cmake/install-config.cmake
    DESTINATION "${widgets-addons_INSTALL_CMAKEDIR}"
    RENAME "${package}Config.cmake"
    COMPONENT widgets-addons_Development
)

install(
    FILES "${PROJECT_BINARY_DIR}/${package}ConfigVersion.cmake"
    DESTINATION "${widgets-addons_INSTALL_CMAKEDIR}"
    COMPONENT widgets-addons_Development
)

install(
    EXPORT widgets-addonsTargets
    NAMESPACE widgets-addons::
    DESTINATION "${widgets-addons_INSTALL_CMAKEDIR}"
    COMPONENT widgets-addons_Development
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
