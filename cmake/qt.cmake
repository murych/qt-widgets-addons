set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(
  QT NAMES Qt6 Qt5
  COMPONENTS Widgets
  REQUIRED)

find_package(
  Qt5
  COMPONENTS Widgets
  REQUIRED)
